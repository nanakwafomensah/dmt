<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Byproduct extends Model
{
    //
    protected $fillable =[
        'name',
        'product_id',
//        'product_name',
        'tank_id',
        'by_tank_id',
        'vessel_id',
        'daily_measure',
        'daily_production',
        'cummulative_production',
        'daily_sale',
        'daily_balance',
        'gov_quantity',
        'remark',
        
    ];
}
