<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $fillable=['name','tank_id','vessel_id'];

    public function tank(){
        return $this->belongsTo('App\Tank');
    }
    public function byproduct(){
        return $this->hasMany('App\Byproduct');
    }
    
}
