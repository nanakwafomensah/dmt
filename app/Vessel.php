<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vessel extends Model
{
    //
    protected  $fillable=['name','amount_crude_oil'];
    
    public function tank(){
        return $this->hasMany(Tank::class);
    }
}
