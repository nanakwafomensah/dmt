<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    //
    protected $fillable =['client_id','request_date','product_id','vessel_id','refined_quantity','sell_quantity'];
}
