<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Load extends Model
{
    //
    protected $fillable = ['client_id','lift_date','product_id','vessel_id','lifted_quantity'];
}
