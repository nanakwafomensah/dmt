<?php
namespace App\Helpers;
use App\Byproduct;
use App\Consignment;
use App\Load;
use App\Product;
use App\Request;
use App\Tank;
use DB;
use Illuminate\Support\Collection;
class AppHelper
{
    public static function getremaining($tank_id,$vessel_id)
    {
        $quantity_in = Tank::where('id',$tank_id)->where('purpose',1)->where('vessel_id',$vessel_id)->pluck('quantity_in')->first();
        $quantity_processed =Consignment::where('tank_id',$tank_id)->where('vessel_id',$vessel_id)->sum('quantity_processed');
        if(sprintf("%.3f", $quantity_in) == sprintf("%.3f", $quantity_processed) ) {
            return 0.000;
        }else{
            return $quantity_in - $quantity_processed;
        }
        
    }

   public static function totalremain($vessel_id){
       $quantity_in = Tank::where('vessel_id',$vessel_id)->where('purpose',1)->sum('quantity_in');
       $quantity_processed =Consignment::where('vessel_id',$vessel_id)->sum('quantity_processed');
       return $quantity_in - $quantity_processed;
   }
    
    public static function summarydailyproduction($tank_id){
        return Byproduct::where('by_tank_id',$tank_id)->sum('daily_production');
    }
    public static function summarydailyproductiontotal(){
        return Byproduct::sum('daily_production');
    }
    public static function summarygovproduction($tank_id){
        return Byproduct::where('by_tank_id',$tank_id)->sum('gov_quantity');
    }
    public static function summarygovproductiontotal(){
        return Byproduct::sum('gov_quantity');
    }





    /****************************--------------Product summary--------******************/
    public static function productsummary_refined($product_id,$vessel_id){

        $tank_id=Product::where('id',$product_id)->where('vessel_id',$vessel_id)->pluck('tank_id')->first();
        return Byproduct::where('by_tank_id',$tank_id)->sum('daily_production') * 1000;
    }
    public static function productsummarygov_refined($product_id,$vessel_id){

        $tank_id=Product::where('id',$product_id)->where('vessel_id',$vessel_id)->pluck('tank_id')->first();
        return Byproduct::where('by_tank_id',$tank_id)->sum('gov_quantity') * 1000;
    }
    public static function productsummary_sold($product_id,$vessel_id){
        return Request::where('product_id',$product_id)->where('vessel_id',$vessel_id)->sum('sell_quantity');

    }
    public static function productsummary_lifted($product_id,$vessel_id){
        return Load::where('product_id',$product_id)->where('vessel_id',$vessel_id)->sum('lifted_quantity');
    }
    public static function productsummary_remainaftersales($product_id,$vessel_id){
        $tank_id=Product::where('id',$product_id)->where('vessel_id',$vessel_id)->pluck('tank_id')->first();
        $totalfined= Byproduct::where('by_tank_id',$tank_id)->sum('gov_quantity');

        return ($totalfined *1000) - Request::where('product_id',$product_id)->where('vessel_id',$vessel_id)->sum('sell_quantity');
    }
    public static function productsummary_remainafterlift($product_id,$vessel_id){
        $tank_id=Product::where('id',$product_id)->where('vessel_id',$vessel_id)->pluck('tank_id')->first();
        $totalfined= Byproduct::where('by_tank_id',$tank_id)->sum('gov_quantity');
        
     // $allrequest = Request::where('product_id',$product_id)->where('vessel_id',$vessel_id)->sum('sell_quantity');
     $alllifted = Load::where('product_id',$product_id)->where('vessel_id',$vessel_id)->sum('lifted_quantity');
        return  ($totalfined*1000) - $alllifted ;
}


    public static function total_summaryrefined($vessel_id){
        return Byproduct::where('vessel_id',$vessel_id)->sum('daily_production') * 1000;
    }
    public static function total_summarygovrefined($vessel_id){
        return Byproduct::where('vessel_id',$vessel_id)->sum('gov_quantity') * 1000;
    }
    public static function total_summarysold($vessel_id){
        return Request::where('vessel_id',$vessel_id)->sum('sell_quantity');

    }
    public static function total_summarylifted($vessel_id){
        return Load::where('vessel_id',$vessel_id)->sum('lifted_quantity');
    }
    public static function total_remainaftersales($vessel_id){
      
        $totalfined= Byproduct::where('vessel_id',$vessel_id)->sum('gov_quantity');

        return ($totalfined *1000) - Request::where('vessel_id',$vessel_id)->sum('sell_quantity');
    }
    public static function total_remainafterlift($vessel_id){
        //$allrequest = Request::where('vessel_id',$vessel_id)->sum('sell_quantity');
        $totalfined= Byproduct::where('vessel_id',$vessel_id)->sum('gov_quantity');
        $alllifted = Load::where('vessel_id',$vessel_id)->sum('lifted_quantity');
        return  ($totalfined*1000) - $alllifted;
    }

    /***************************-------------End Product Summary------********************/

    
    /***************not in use ************---------------stock sales-----------**********************/
    public static function  getclientRequest($client_id,$vessel_id){
        $newCollection = new Collection([]);
        $returnrequest=new Collection([]);
       
        $allproductfrombyproducttable=Byproduct::where('vessel_id',$vessel_id)->get();
        foreach ($allproductfrombyproducttable as $s){
            $newCollection->push($s->product_id);
        }
        $product_id=$newCollection->unique()->values()->all();

        for($count=0;$count < count($product_id); $count++){

         $values=   \App\Request::where('client_id',$client_id)
                ->where('vessel_id',$vessel_id)
                ->where('product_id',$product_id[$count])
                ->sum('sell_quantity');

            $returnrequest->push($values);

        }
   
        return $returnrequest;

    }
}