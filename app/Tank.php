<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tank extends Model
{
    //
    protected $fillable =['vessel_id','name','quantity_in','gov_quantity','remark','purpose','description','opcost'];
    
    public function product(){
         return $this->hasOne('App\Product');
    }
}
