<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consignment extends Model
{
    //
    protected $fillable=[
        'name',
        'vessel_id',
        'tank_id',
        'quantity_processed',
    
        
        ];
}
