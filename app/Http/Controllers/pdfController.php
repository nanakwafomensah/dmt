<?php

namespace App\Http\Controllers;

use App\Byproduct;
use App\Consignment;
use App\Product;
use App\Tank;
use Illuminate\Http\Request;
Use PDF;
use DB;
use Sentinel;
use Illuminate\Support\Collection;
use Symfony\Component\HttpKernel\Client;

class pdfController extends Controller
{
    //

    public function index(Request $request){

        $report_type= $request->report_type;
        $date=$request->date;
        $vessel_id=$request->vessel_id;
        $remark=$request->remark;

         if($report_type == '1'){
             $pdfname='TankSummary'.date('Y-M-d');
            $quantity_in_tank=Tank::where('purpose',1)->where('vessel_id',$vessel_id)->get();
            $byproduct=Tank::where('vessel_id',$vessel_id)->where('purpose',2)->orWhere('purpose',3)->get();
              
            $html = view('pdfs.tanksummary')->with([
                'quantity_in_tank'=>$quantity_in_tank,
                'byproduct'=>$byproduct,
                'date'=>$date,
                'vessel_id'=>$vessel_id,
                'remark'=>$remark,
            ])->render();


        }
         elseif ($report_type == '2'){
             $pdfname='SalesStock'.date('Y-M-d');
           $newCollection = new Collection([]);
           $allproductfrombyproducttable=Byproduct::where('vessel_id',$vessel_id)->get();
            foreach ($allproductfrombyproducttable as $s){
                $newCollection->push($s->product_id);
            }
           $x=$newCollection->unique()->values()->all();

          /**************client request*****/  
            $newCollection = new Collection([]);
            $allproductfrombyproducttable=Byproduct::where('vessel_id',$vessel_id)->get();
            foreach ($allproductfrombyproducttable as $s){
                $newCollection->push($s->product_id);
            }

            $product_id=$newCollection->unique()->values()->all();

            $clientrequest=array();
            $clientrequest2=array();
            $totals=array();
            $totals2=array();

            foreach (\App\Client::all() as $c){
                $returnrequest=array();
                $returnrequest2=array();
                array_push($returnrequest,$c->name);
                array_push($returnrequest2,$c->name);


                for($count=0;$count < count($product_id); $count++){
                      if($product_id[$count] <= 9) {
                          $values = \App\Request::where('client_id', $c->id)
                              ->where('vessel_id', $vessel_id)
                              ->where('product_id', $product_id[$count])
                              ->sum('sell_quantity');
                          $load = \App\Load::where('client_id', $c->id)
                              ->where('vessel_id', $vessel_id)
                              ->where('product_id', $product_id[$count])
                              ->sum('lifted_quantity');

                          $remain = $values - $load;
                          array_push($returnrequest, $values . '|' . $load . '|' . $remain);
                      }else{
                          $values = \App\Request::where('client_id', $c->id)
                              ->where('vessel_id', $vessel_id)
                              ->where('product_id', $product_id[$count])
                              ->sum('sell_quantity');
                          $load = \App\Load::where('client_id', $c->id)
                              ->where('vessel_id', $vessel_id)
                              ->where('product_id', $product_id[$count])
                              ->sum('lifted_quantity');

                          $remain = $values - $load;
                          array_push($returnrequest2,$values.'|'.$load.'|'.$remain);
                      }
                          
                      
                    
                    
                }
                array_push($clientrequest,$returnrequest);
                array_push($clientrequest2,$returnrequest2);

            }

            
            for($count=0;$count < count($product_id); $count++){
                if($product_id[$count] <= 9) {
                    $values = \App\Request::where('vessel_id', $vessel_id)
                        ->where('product_id', $product_id[$count])
                        ->sum('sell_quantity');
                    $totalloading = \App\Load::where('vessel_id', $vessel_id)
                        ->where('product_id', $product_id[$count])
                        ->sum('lifted_quantity');
                    $values1 = \App\Request::where('vessel_id', $vessel_id)
                        ->where('product_id', $product_id[$count])
                        ->sum('sell_quantity');
                    $values2 = \App\Load::where('vessel_id', $vessel_id)
                        ->where('product_id', $product_id[$count])
                        ->sum('lifted_quantity');
                    $rem = $values1 - $values2;

                    array_push($totals, $values . "|" . $totalloading . "|" . $rem);
                }else{
                    $values = \App\Request::where('vessel_id', $vessel_id)
                        ->where('product_id', $product_id[$count])
                        ->sum('sell_quantity');
                    $totalloading = \App\Load::where('vessel_id', $vessel_id)
                        ->where('product_id', $product_id[$count])
                        ->sum('lifted_quantity');
                    $values1 = \App\Request::where('vessel_id', $vessel_id)
                        ->where('product_id', $product_id[$count])
                        ->sum('sell_quantity');
                    $values2 = \App\Load::where('vessel_id', $vessel_id)
                        ->where('product_id', $product_id[$count])
                        ->sum('lifted_quantity');
                    $rem = $values1 - $values2;

                    array_push($totals2, $values . "|" . $totalloading . "|" . $rem);
                }
            }
           /*******client request*******/


            $html = view('pdfs.stocksales')->with([

                'date'=>$date,
                'vessel_id'=>$vessel_id,
                'clientrequest'=>$clientrequest,
                'clientrequest2'=>$clientrequest2,
                'totals'=>$totals,
                'totals2'=>$totals2,
                'x'=>$x,
                'remark'=>$remark,
            ])->render();

        }
         elseif ($report_type == '3'){
             $pdfname='ProductSummary'.date('Y-M-d');
            $products=Product::all()->where('vessel_id',$vessel_id);
            $html = view('pdfs.productsummary')->with([
                'products'=>$products,
                'date'=>$date,
                'vessel_id'=>$vessel_id,
                'remark'=>$remark,
            ])->render();
        }
         elseif ($report_type == '4'){
             $pdfname='Cummulative Summary'.date('Y-M-d');
             $alltank=Tank::where('vessel_id',$vessel_id)->where('purpose',1)->orWhere('purpose',2)->orderBy('purpose', 'desc')->get();
             $xcrude=Tank::where('vessel_id',$vessel_id)->where('purpose',1)->orderBy('purpose', 'desc')->get();
            // dd($xcrude);
             $xyield=Tank::where('vessel_id',$vessel_id)->where('purpose',2)->orWhere('purpose',3)->orderBy('purpose', 'desc')->get();

             /*************crude*******/
             $tank_id_crude = new Collection([]);
             $tank_id_yield = new Collection([]);
             $dataset=array();
             foreach ($xcrude as $s){
                 $tank_id_crude->push($s->id);
             }
             $tank_idcrude=$tank_id_crude->unique()->values()->all();

             /*************crude*******/
             /*************yield*******/

             foreach ($xyield as $s){
                 $tank_id_yield->push($s->id);
             }
             $tank_idyield=$tank_id_yield->unique()->values()->all();

             /*************yield*******/
             $arry=array();


             foreach (\App\Consignment::all() as $c){
                 $rowdata=array();
                 array_push($rowdata,$c->name);

                 for($count=0;$count < count($tank_idcrude); $count++){
                     $quantity_processed=   \App\Consignment::where('vessel_id',$vessel_id)
                             ->where('tank_id',$tank_idcrude[$count])->where('name','<=',$c->name)->sum('quantity_processed') ;
                     array_push($rowdata,$quantity_processed);
                 }
                 for($count=0;$count < count($tank_idyield); $count++){
                     $quantity_processed=   \App\Byproduct::where('vessel_id',$vessel_id)
                         ->where('by_tank_id',$tank_idyield[$count])->where('name','<=',$c->name)->sum('daily_production') ;
                     array_push($rowdata,$quantity_processed);
                 }

              array_push($arry,$rowdata);
             }

               


             $html = view('pdfs.cummulativesummary')->with([
                 'vessel_id'=>$vessel_id,
                 'date'=>$date,
                 'alltank'=>$alltank,

                 'xcrude'=>$xcrude,
                 'xyield'=>$xyield,
                 'arry'=>$arry,
                 'remark'=>$remark,
             ])->render();
         }

        $pdf= new  PDF();
        $pdf::filename($pdfname.'.pdf');
        return $pdf::load($html, 'A3', 'landscape')->show();
      //  return PDF::load($html, 'A4', 'landscape')->show();
   
    }


    public function reportother(Request $request){
        $vessel_id=$request->vessel_id;
        if($request->report_type=='1'){//all request
            $requestss=\App\Request::whereDate('created_at','>=',$request->fromdate)
                ->whereDate('created_at','<=',$request->todate)
                ->orderBy('created_at', 'ASC')
                ->get();

            $html = view('pdfs.allrequests')->with([
                'requestss'=>$requestss,
                'vessel_id'=>$vessel_id
            ])->render();
        }
        elseif ($request->report_type=='2'){//all liftings
            $liftings=\App\Load::whereDate('created_at','>=',$request->fromdate)->whereDate('created_at','<=',$request->todate)->orderBy('created_at', 'ASC')->get();

            $html = view('pdfs.allliftings')->with([
                  'lifting'=>$liftings,
                 'vessel_id'=>$vessel_id,
            ])->render();

        }
        elseif ($request->report_type=='3'){//all crude oil in tanks
            $tank_id=$request->tank_id;
            $tankdata=\App\Consignment::where('tank_id',$request->tank_id)->where('vessel_id',$request->vessel_id)->whereDate('created_at','>=',$request->fromdate)
                ->whereDate('created_at','<=',$request->todate)->orderBy('created_at', 'ASC')->get();

            $html = view('pdfs.alltankdata')->with([
                  'tankdata'=>$tankdata,
                  'tank_id'=>$tank_id,
'vessel_id'=>$vessel_id,
            ])->render();

        }
        elseif ($request->report_type=='4'){
            $product_id=$request->product_id;
            $productdata=Byproduct::where('product_id',$request->product_id)->where('vessel_id',$request->vessel_id)->whereDate('created_at','>=',$request->fromdate)
                ->whereDate('created_at','<=',$request->todate)->orderBy('created_at', 'ASC')->get();


            $html = view('pdfs.allby')->with([
                'productdata'=>$productdata,
                'product_id'=>$product_id,
'vessel_id'=>$vessel_id,
            ])->render();
        }
        elseif ($request->report_type=='5'){
         $product_id=$request->product_id;
            $productdata=Byproduct::where('product_id',$request->product_id)->where('vessel_id',$request->vessel_id)->whereDate('created_at','>=',$request->fromdate)
                ->whereDate('created_at','<=',$request->todate)->orderBy('created_at', 'ASC')->get();


            $html = view('pdfs.allrefined')->with([
                'productdata'=>$productdata,
                'product_id'=>$product_id,
                'vessel_id'=>$vessel_id,

            ])->render();
        }
        return PDF::load($html, 'A3', 'landscape')->show();
        
    }
}
