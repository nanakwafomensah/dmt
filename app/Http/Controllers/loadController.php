<?php

namespace App\Http\Controllers;

use App\Load;
use Illuminate\Http\Request;

class loadController extends Controller
{
    //
    public  function index(){
        $liftings=Load::all();
        return view('load')->with(['liftings'=>$liftings]);
    }
    public function save(Request $request){
       // dd($request->all());
        $re = new Load();
        $re->client_id = $request->client_id;
        $re->lift_date = $request->lift_date;
        $re->product_id = $request->product_id;
        $re->vessel_id =$request->vessel_id;
        $re->lifted_quantity = $request->lifted_quantity;
        $re->save();
        return redirect('load')->withInput();
    }
    public function update(Request $request){
      
        $re =Load::find($request->idEdit);

        $re->client_id = $request->client_idEdit;
        $re->lift_date = $request->lift_dateEdit;
        $re->product_id = $request->product_idEdit;
        $re->vessel_id = $request->vessel_idEdit;
        $re->lifted_quantity = $request->lifted_quantityEdit;


        $re->save();
        return redirect('load');
    }
    public function delete(Request $request){
        Load::find($request->idDelete)->delete();
        return redirect('load');
    }
    public function product_request(Request $request){
        $amount_requested=\App\Request::where('client_id',$request->client_id)->where('product_id',$request->product_id)->where('vessel_id',$request->vessel_id)->sum('sell_quantity');
        $amount_loaded=\App\Load::where('client_id',$request->client_id)->where('product_id',$request->product_id)->where('vessel_id',$request->vessel_id)->sum('lifted_quantity');
        return $amount_requested - $amount_loaded;
    }
}
