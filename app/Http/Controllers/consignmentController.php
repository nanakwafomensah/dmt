<?php

namespace App\Http\Controllers;

use App\Byproduct;
use App\Consignment;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class consignmentController extends Controller
{
    //
    public  function index(){
        $consignments= Consignment::orderBy('created_at')->get();;
        return view('consignment')->with(['consignments'=>$consignments]);
    }
    public function save(Request $request){
        Consignment::create($request->all());
        return redirect('consignment')->withInput();
    }
    public function update(Request $request){
       $consignment =Consignment::find($request->idEdit);
        $consignment->name = $request->nameEdit;
        $consignment->tank_id = $request->tank_idEdit;
        $consignment->vessel_id = $request->vessel_idEdit;
        $consignment->quantity_processed = $request->quantity_processedEdit;
       
        $consignment->save();
        return redirect('consignment');
    }
    public function delete(Request $request){

        Consignment::find($request->idDelete)->delete();
        return redirect('consignment');
    }
    public function consignmentstartedonTank(Request $request){
        $consignmentontank = Consignment::where('tank_id',$request->tank_id)->first();
        if ($consignmentontank === null) {
            return  '0';
        }
        else{
            $consignmentontank = Consignment::all()->sum('cummulative_processed');
            return $consignmentontank;
        }
    }
}
