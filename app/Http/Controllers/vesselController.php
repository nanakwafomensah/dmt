<?php

namespace App\Http\Controllers;

use App\Vessel;
use Illuminate\Http\Request;

class vesselController extends Controller
{
    //
    public  function index(){
        $vessels= Vessel::all();
        return view('vessel')->with(['vessels'=>$vessels]);
    }
    public function save(Request $request){
        Vessel::create($request->all());
        return redirect('vessel');
    }
    public function update(request $request){
        $vessel =Vessel::find($request->idEdit);
        
        $vessel->name = $request->nameEdit;
        $vessel->amount_crude_oil = $request->amount_crude_oilEdit;
        $vessel->save();
        return redirect('vessel');
    }
    public function delete(Request $request){
        Vessel::find($request->idDelete)->delete();
        return redirect('vessel');
    }
}
