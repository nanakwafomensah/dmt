<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

class clientController extends Controller
{
    //
    public  function index(){
        $clients=Client::all();
        return view('client')->with(['clients'=>$clients]);
    }
    public function save(Request $request){
        Client::create($request->all());
        return redirect('client');
    }
    public function update(Request $request){
        $client =Client::find($request->idEdit);
        $client->name = $request->nameEdit;

        $client->save();
        return redirect('client');
    }
    public function delete(Request $request){
        Client::find($request->idDelete)->delete();
        return redirect('client');
    }
}
