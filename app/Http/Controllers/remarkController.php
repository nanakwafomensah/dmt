<?php

namespace App\Http\Controllers;

use App\Remark;
use Illuminate\Http\Request;

class remarkController extends Controller
{
    //
    public function index(){
        $remarks=Remark::all();
        return view('remark')->with(['remarks'=>$remarks]);
      
    }
    public function save(Request $request){
        Remark::create($request->all());
        return redirect('remark');
    }
    public function update(Request $request){
        $re =Remark::find($request->idEdit);
        $re->description = $request->descriptionEdit;
        return redirect('remark');
    }
    public function delete(Request $request){
        Remark::find($request->idDelete)->delete();
        return redirect('remark');
    }
}
