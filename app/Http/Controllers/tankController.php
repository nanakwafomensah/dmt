<?php

namespace App\Http\Controllers;

use App\Consignment;
use App\Tank;
use Illuminate\Http\Request;

class tankController extends Controller
{
    //
    public  function index(){
        $tanks= Tank::all();
        return view('tank')->with(['tanks'=>$tanks]);
    }
    public function save(Request $request){
        Tank::create($request->all());
        return redirect('tank');
    }
    public function update(Request $request){
        $tank =Tank::find($request->idEdit);
        $tank->name = $request->nameEdit;
        $tank->vessel_id = $request->vessel_idEdit;
        $tank->quantity_in = $request->quantity_inEdit;
        $tank->gov_quantity = $request->gov_quantityEdit;
        $tank->purpose = $request->purposeEdit;
        $tank->description = $request->descriptionEdit;
        $tank->opcost = $request->opcostEdit;
        $tank->remark = $request->remarkEdit;

        $tank->save();
        return redirect('tank');
    }
    public function delete(Request $request){
        Tank::find($request->idDelete)->delete();
        return redirect('tank');
    }
    public function  getquantityreceived(Request $request){
        $consignmentontank = Consignment::orderBy('id', 'desc')->select('opening_quantity')->first();
        if ($consignmentontank === null) {
            $quantity_in= Tank::find($request->tank_id)->first();
            return $quantity_in->quantity_in."|0";
        }
        else{
            return $consignmentontank->opening_quantity."|1";
        }
//
    }

    public function bytankidselectbox()
    {
        $outputtankdropdown='<option value="">Select Tank</option>';
        $tank=Tank::all()->where('purpose',2)->orwhere('purpose',3);
        foreach ($tank as $p){
            $outputtankdropdown.='<option value="'.$p->id.'">'.$p->name.'</option>';
        }

        return Response($outputtankdropdown);

    }
    public function selecttankbyproductid($product_id){
        return \App\Product::find($product_id)->tank_id;

    }
}
