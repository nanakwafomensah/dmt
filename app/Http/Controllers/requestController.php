<?php

namespace App\Http\Controllers;

use App\Consignment;
use App\Tank;
use Illuminate\Http\Request;

class requestController extends Controller
{
    //
    public  function index(){
        $requests = \App\Request::orderBy('created_at')->get();
        return view('request')->with(['requests'=>$requests]);
    }
    public function save(Request $request){
        \App\Request::create($request->all());
        return redirect('request')->withInput();
    }
    public function update(Request $request){
        $re =\App\Request::find($request->idEdit);
        $re->client_id = $request->client_idEdit;
        $re->request_date = $request->request_dateEdit;
        $re->product_id = $request->product_idEdit;
        $re->vessel_id = $request->vessel_idEdit;
        $re->refined_quantity = $request->refined_quantityEdit;
        $re->sell_quantity = $request->sell_quantityEdit;
       

        $re->save();
        return redirect('request');
    }
    public function delete(Request $request){
        \App\Request::find($request->idDelete)->delete();
        return redirect('request');
    }
    
    
    public function quantity_refined_for_product(Request $request){
      
       $quantity_processed =\App\Byproduct::where('product_id',$request->product_id)->sum('gov_quantity');

       $quantity_requested= \App\Request::where('product_id',$request->product_id)->sum('sell_quantity');

        return $quantity_processed *1000 ."|".$quantity_requested;

    }
}
