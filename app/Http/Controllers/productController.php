<?php

namespace App\Http\Controllers;

use App\Product;
use App\Tank;
use Illuminate\Http\Request;

class productController extends Controller
{
    //
    
    public  function index(){
        $products= Product::all();
        return view('product')->with(['products'=>$products]);
    }
    public function save(Request $request){
        $p = new Product();
        $p->name=$request->name;
        $p->tank_id=$request->tank_id;
        $p->vessel_id=Tank::find($request->tank_id)->vessel_id;
        $p->save();
        return redirect('product');
    }
    public function update(Request $request){
        $product =Product::find($request->idEdit);
        $product->name = $request->nameEdit;
        $product->tank_id = $request->tank_idEdit;
        $product->vessel_id =Tank::find($request->tank_idEdit)->vessel_id;
        
        $product->save();
        return redirect('product');
    }
    public function delete(Request $request){
        Product::find($request->idDelete)->delete();
        return redirect('product');
    }
    
    public function  productselectbox(){
        $outputproductdropdown='<option value="">Select product</option>';
        $product=Product::all();
        foreach ($product as $p){
            $outputproductdropdown.='<option value="'.$p->id.'">'.$p->name.'</option>';
        }

        return Response($outputproductdropdown);
    }
}
