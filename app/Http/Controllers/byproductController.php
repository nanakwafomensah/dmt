<?php

namespace App\Http\Controllers;

use App\Byproduct;
use App\Product;
use Illuminate\Http\Request;
use DB;
class byproductController extends Controller
{
    //
    public function index(){
        return view('byproduct');
    }
    public function indexrefined(){
        return view('refined');
    }
    
    public function save(Request $request){
        Byproduct::create($request->all());
        return redirect('refined')->withInput();
    }
    public function update(Request $request){
      
        $byproduct = Byproduct::find($request->idEdit);
        $byproduct->name = $request->nameEdit;
        $byproduct->product_id = $request->product_idEdit;
//        $byproduct->product_name = $request->product_nameEdit;
        $byproduct->tank_id = $request->tank_idEdit;
        $byproduct->by_tank_id = $request->by_tank_idEdit;
        $byproduct->vessel_id = $request->vessel_idEdit;
        $byproduct->daily_measure = $request->daily_measureEdit;
        $byproduct->daily_production = $request->daily_productionEdit;
        $byproduct->daily_sale = $request->daily_saleEdit;
        $byproduct->gov_quantity = $request->gov_quantityEdit;
        $byproduct->remark = $request->remarkEdit;
        $byproduct->save();
        return redirect('refined');
    }
    public function delete(Request $request){
        Byproduct::find($request->idDelete)->delete();
        return redirect('refined');
    }

    public function getcummulatedproduction(Request $request){
        $cummulatedrecord=Byproduct::where('by_tank_id',$request->by_tank_id)->where('tank_id',$request->tank_id)->first();
        if ($cummulatedrecord === null) {
            return 0;
        }else{
            $consignmentontank = Byproduct::orderBy('id', 'desc')->select('cummulative_production')->where('by_tank_id',$request->by_tank_id)->where('tank_id',$request->tank_id)->first();
            return $consignmentontank->cummulative_production;
        }
    }
    
    public function getByproductfortank($date,$tank_id){
       
        $byproduct = Byproduct::where('name',$date)->where('tank_id',$tank_id)->get();

        return json_encode($byproduct);

    }
    public  function getProductName($product_id){
        return  Product::find($product_id)->name;
    }
    public function getproductdetails(Request $request){
        $byproduct=Byproduct::where('product_id',$request->product_id)->get();
        return json_encode($byproduct);
    }
}
