<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::get('tank',['as'=>'tank','uses'=>'tankController@index']);
Route::get('remark',['as'=>'remark','uses'=>'remarkController@index']);
Route::get('vessel',['as'=>'vessel','uses'=>'vesselController@index']);
Route::get('client',['as'=>'client','uses'=>'clientController@index']);
Route::get('request',['as'=>'request','uses'=>'requestController@index']);
Route::get('load',['as'=>'load','uses'=>'loadController@index']);
Route::get('product',['as'=>'product','uses'=>'productController@index']);
Route::get('consignment',['as'=>'consignment','uses'=>'consignmentController@index']);
Route::get('userutil',['as'=>'userutil','uses'=>'userutilController@index']);
Route::get('report',['as'=>'report','uses'=>'reportController@index']);
Route::get('reportother',['as'=>'reportother','uses'=>'reportController@reportother']);
Route::post('downloadpdf',['as'=>'downloadpdf','uses'=>'pdfController@index']);
Route::get('refined',['as'=>'refined','uses'=>'byproductController@indexrefined']);
Route::post('getquantityreceived',['as'=>'getquantityreceived','uses'=>'tankController@getquantityreceived']);
Route::post('consignmentstartedonTank',['as'=>'consignmentstartedonTank','uses'=>'consignmentController@consignmentstartedonTank']);
Route::post('getcummulatedproduction',['as'=>'getcummulatedproduction','uses'=>'byproductController@getcummulatedproduction']);
Route::post('getproductdetails',['as'=>'getproductdetails','uses'=>'byproductController@getproductdetails']);
Route::post('quantity_refined_for_product',['as'=>'quantity_refined_for_product','uses'=>'requestController@quantity_refined_for_product']);
Route::post('product_request',['as'=>'product_request','uses'=>'loadController@product_request']);
Route::get('byproduct',['as'=>'byproduct','uses'=>'byproductController@index']);


Route::get('getByproductfortank/{date}/{tank_id}',['as'=>'getByproductfortank/{date}/{tank_id}','uses'=>'byproductController@getByproductfortank']);
Route::get('selecttankbyproductid/{product_id}',['as'=>'selecttankbyproductid/{product_id}','uses'=>'tankController@selecttankbyproductid']);
Route::get('getProductName/{product_id}',['as'=>'getProductName/{product_id}','uses'=>'byproductController@getProductName']);


Route::post('saveconsignment',['as'=>'saveconsignment','uses'=>'consignmentController@save']);
Route::post('updateconsignment',['as'=>'updateconsignment','uses'=>'consignmentController@update']);
Route::post('deleteconsignment',['as'=>'deleteconsignment','uses'=>'consignmentController@delete']);

Route::post('saveclient',['as'=>'saveclient','uses'=>'clientController@save']);
Route::post('updateclient',['as'=>'updateclient','uses'=>'clientController@update']);
Route::post('deleteclient',['as'=>'deleteclient','uses'=>'clientController@delete']);

Route::post('saverefined',['as'=>'saverefined','uses'=>'byproductController@save']);
Route::post('updaterefined',['as'=>'updaterefined','uses'=>'byproductController@update']);
Route::post('deleterefined',['as'=>'deleterefined','uses'=>'byproductController@delete']);

Route::post('saverequest',['as'=>'saverequest','uses'=>'requestController@save']);
Route::post('updaterequest',['as'=>'updaterequest','uses'=>'requestController@update']);
Route::post('deleterequest',['as'=>'deleterequest','uses'=>'requestController@delete']);

Route::post('saveload',['as'=>'saveload','uses'=>'loadController@save']);
Route::post('updateload',['as'=>'updateload','uses'=>'loadController@update']);
Route::post('deleteload',['as'=>'deleteload','uses'=>'loadController@delete']);

Route::post('saveproduct',['as'=>'saveproduct','uses'=>'productController@save']);
Route::post('updateproduct',['as'=>'updateproduct','uses'=>'productController@update']);
Route::post('deleteproduct',['as'=>'deleteproduct','uses'=>'productController@delete']);

Route::post('savetank',['as'=>'savetank','uses'=>'tankController@save']);
Route::post('updatetank',['as'=>'updatetank','uses'=>'tankController@update']);
Route::post('deletetank',['as'=>'deletetank','uses'=>'tankController@delete']);

Route::post('saveremark',['as'=>'saveremark','uses'=>'remarkController@save']);
Route::post('updateremark',['as'=>'updateremark','uses'=>'remarkController@update']);
Route::post('deleteremark',['as'=>'deleteremark','uses'=>'remarkController@delete']);

Route::post('savevessel',['as'=>'savevessel','uses'=>'vesselController@save']);
Route::post('updatevessel',['as'=>'updatevessel','uses'=>'vesselController@update']);
Route::post('deletevessel',['as'=>'deletevessel','uses'=>'vesselController@delete']);

Route::post('login',['as'=>'login','uses'=>'loginController@postLogin']);

Route::post('logout',['as'=>'logout','uses'=>'loginController@postlogout']);
Route::post('saveuser',['as'=>'saveuser','uses'=>'userutilController@save']);


Route::get('bytankidselectbox',['as'=>'bytankidselectbox','uses'=>'tankController@bytankidselectbox']);
Route::get('productselectbox',['as'=>'productselectbox','uses'=>'productController@productselectbox']);


Route::post('reportother',['as'=>'reportother','uses'=>'pdfController@reportother']);