<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DMT crude Oil Processing</title>
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta property="og:url" content="">
    <meta property="og:type" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:creator" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
    <link rel="apple-touch-icon" sizes="180x180" href="">
    <link rel="icon" type="image/png" href="favicon.ico" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon.ico" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#27ae60">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="assets/css/vendor.min.css">
    <link rel="stylesheet" href="assets/css/elephant.min.css">
    <link rel="stylesheet" href="assets/css/application.min.css">
    <link rel="stylesheet" href="assets/css/demo.min.css">
    <link rel="stylesheet" href="assets/parsley/css/parsley.css">
</head>
<body class="layout layout-header-fixed">
<div class="layout-header">
    @include('template.topbar')
</div>
<div class="layout-main">
    <div class="layout-sidebar" >
        <div class="layout-sidebar-backdrop" style="background-color: #217345;"></div>
        <div class="layout-sidebar-body" style="background-color: #217345;">
            <div class="custom-scrollbar" >
                <nav id="sidenav" class="sidenav-collapse collapse" style="background-color: #217345; color: #fff">
                    <ul class="sidenav">
                        <li class="sidenav-heading">Production Setting</li>
                        <li class="sidenav-item ">
                            <a href="vessel" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-truck"></span>
                                <span class="sidenav-label" style="font-size: 11px">Vessel</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="tank" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-beer"></span>
                                <span class="sidenav-label" style="font-size: 11px">Tank</span>
                            </a>

                        </li>

                        <li class="sidenav-item ">
                            <a href="product" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-group"></span>
                                <span class="sidenav-label" style="font-size: 11px">Refined Product</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="client" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-user"></span>
                                <span class="sidenav-label" style="font-size: 11px">Off Takers</span>
                            </a>

                        </li>




                        <li class="sidenav-heading">Depot Record</li>
                        <li class="sidenav-item ">
                            <a href="consignment" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">New Crude Oil </span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="refined" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">New Product</span>
                            </a>

                        </li>

                        <li class="sidenav-heading">Sale</li>
                        <li class="sidenav-item">
                            <a href="request" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-archive"></span>
                                <span class="sidenav-label" style="font-size: 11px">New Release</span>
                            </a>
                        </li>
                        <li class="sidenav-item">
                            <a href="load" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-suitcase"></span>
                                <span class="sidenav-label" style="font-size: 11px">Loading</span>
                            </a>
                        </li>
                        <li class="sidenav-heading">Report</li>
                        <li class="sidenav-item ">
                            <a href="report" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Summary Report</span>
                            </a>
                        </li>
                        <li class="sidenav-item active">
                            <a href="reportother" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Other Report</span>
                            </a>
                        </li>
                        <li class="sidenav-item">
                            <a href="remark" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Remarks</span>
                            </a>
                        </li>


                        <li class="sidenav-heading">User Management</li>

                        <li class="sidenav-item ">
                            <a href="#createUser" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-user"></span>
                                <span class="sidenav-label" style="font-size: 11px" data-toggle="modal" data-target="#createUser">Create New User</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="userutil" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-edit"></span>
                                <span class="sidenav-label" style="font-size: 11px" >User Utilities</span>
                            </a>

                        </li>

                    </ul>
                </nav>

            </div>
        </div>
    </div>
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">

                <h1 class="title-bar-title">
                    <span class="d-ib">Search Reports By :</span>
                </h1>
                <p class="title-bar-description">
                    <small  style ="color:#217345">Insights &amp Analytics</small>

                </p>
            </div><br><br>
            <div class = "row">
                <form action="reportother" method="post">
                    {{csrf_field()}}
                    <div class="col-lg-3">

                        <div class="card">

                            <div class="card-header no-border">
                                <div class="col-md-12">
                                    <div class="demo-form-wrapper">


                                        <h3>Sales</h3>

                                        <div class="form-group">
                                            <label for="demo-select2-6" class="form-label" style="font-size: 11px"><u>Report Type</u></label>
                                            <select id="com_change" class="form-control" style="font-size: 11px" name="report_type" required>
                                                <option value="" >Select Type</option>
                                                <option value="1">REQUESTS</option>
                                                <option value="2">LIFTINGS</option>
                                                {{--<option value="3">CRUDE OIL IN TANK</option>--}}
                                                {{--<option value="4">BYPRODUCT </option>--}}
                                                {{--<option value="5">REFINED PRODUCT </option>--}}


                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="demo-select2-6" class="form-label" style="font-size: 11px"><u> PRODUCTION NAME</u></label>
                                                <select id="com_change" class="form-control" style="font-size: 11px" name="vessel_id" required>
                                                    <option value=""  >Select Name</option>
                                                    @foreach(\App\Vessel::all() as $v)
                                                        <option value="{{$v->id}}"  >{{$v->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <br>

                                        </div>
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="demo-select2-6" class="form-label" style="font-size: 11px"><u>FROM DATE</u></label>
                                                <input id="com_change" type="date" name="fromdate" class="form-control" style="font-size: 11px" required/>
                                            </div>
                                            <br>
                                            <div class="form-group">
                                                <label for="demo-select2-6" class="form-label" style="font-size: 11px"><u>TO DATE</u></label>
                                                <input id="com_change" type="date" name="todate" class="form-control" style="font-size: 11px" required/>
                                            </div>
                                            <br>

                                        </div>

                                        <input type="submit" class="btn btn-success form-control" value="View " style="font-size: 11px"/>


                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </form>
                    <form action="reportother" method="post">
                        {{csrf_field()}}
                    <div class="col-lg-3">

                        <div class="card">

                            <div class="card-header no-border">
                                <div class="col-md-12">
                                    <div class="demo-form-wrapper">


                                        <h3>Crude Oil In Tank</h3>

                                        <div class="form-group">
                                            <input name="report_type"  value="3" type="hidden"/>

                                        </div>
                                        <div class="form-group">
                                            <label for="demo-select2-6" class="form-label" style="font-size: 11px"><u>Tank</u></label>
                                            <select id="tank_id" class="form-control" style="font-size: 11px" name="tank_id" required>
                                                <option value="" >Select Type</option>
                                                @foreach(\App\Tank::where('purpose',1)->orWhere('purpose',3)->get() as $t)
                                                    <option value="{{$t->id}}" >{{$t->name ."-----".$t->description }}</option>

                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="demo-select2-6" class="form-label" style="font-size: 11px"><u> PRODUCTION NAME</u></label>
                                                <select id="com_change" class="form-control" style="font-size: 11px" name="vessel_id" required>
                                                    <option value=""  >Select Name</option>
                                                    @foreach(\App\Vessel::all() as $v)
                                                        <option value="{{$v->id}}"  >{{$v->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <br>

                                        </div>
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="demo-select2-6" class="form-label" style="font-size: 11px"><u>FROM DATE</u></label>
                                                <input id="com_change" type="date" name="fromdate" class="form-control" style="font-size: 11px" required/>
                                            </div>
                                            <br>
                                            <div class="form-group">
                                                <label for="demo-select2-6" class="form-label" style="font-size: 11px"><u>TO DATE</u></label>
                                                <input id="com_change" type="date" name="todate" class="form-control" style="font-size: 11px" required/>
                                            </div>
                                            <br>

                                        </div>

                                        <input type="submit" class="btn btn-success form-control" value="View " style="font-size: 11px"/>


                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    </form>
                    <form action="reportother" method="post">
                        {{csrf_field()}}
                    <div class="col-lg-3">

                        <div class="card">

                            <div class="card-header no-border">
                                <div class="col-md-12">
                                    <div class="demo-form-wrapper">



                                        <h3>Refined Product</h3>
                                        <input name="report_type"  value="5" type="hidden"/>

                                        <div class="form-group">
                                            <label for="demo-select2-6" class="form-label" style="font-size: 11px"><u>Product</u></label>
                                            <select id="product_id" class="form-control" style="font-size: 11px" name="product_id" required>
                                                <option value="" >Select Type</option>
                                                @foreach(\App\Product::all() as $t)
                                                    <option value="{{$t->id}}" >{{$t->name}}</option>

                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="demo-select2-6" class="form-label" style="font-size: 11px"><u> PRODUCTION NAME</u></label>
                                                <select id="com_change" class="form-control" style="font-size: 11px" name="vessel_id" required>
                                                    <option value=""  >Select Name</option>
                                                    @foreach(\App\Vessel::all() as $v)
                                                        <option value="{{$v->id}}"  >{{$v->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <br>

                                        </div>
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="demo-select2-6" class="form-label" style="font-size: 11px"><u>FROM DATE</u></label>
                                                <input id="com_change" type="date" name="fromdate" class="form-control" style="font-size: 11px" required/>
                                            </div>
                                            <br>
                                            <div class="form-group">
                                                <label for="demo-select2-6" class="form-label" style="font-size: 11px"><u>TO DATE</u></label>
                                                <input id="com_change" type="date" name="todate" class="form-control" style="font-size: 11px" required/>
                                            </div>
                                            <br>

                                        </div>

                                        <input type="submit" class="btn btn-success form-control" value="View " style="font-size: 11px"/>


                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    </form>
                    <form action="reportother" method="post">
                        {{csrf_field()}}
                    <div class="col-lg-3">

                        <div class="card">

                            <div class="card-header no-border">
                                <div class="col-md-12">
                                    <div class="demo-form-wrapper">


                                        <h3>By - Product</h3>
                                        <input name="report_type"  value="4" type="hidden"/>


                                        <div class="form-group">
                                            <label for="demo-select2-6" class="form-label" style="font-size: 11px"><u>Product</u></label>
                                            <select id="product_id" class="form-control" style="font-size: 11px" name="product_id" required>
                                                <option value="" >Select Type</option>
                                                @foreach(\App\Tank::where('purpose',3)->get() as $t)
                                                    <option value="{{\App\Tank::find($t->id)->product->id}}" >{{\App\Tank::find($t->id)->product->name}}</option>

                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="demo-select2-6" class="form-label" style="font-size: 11px"><u> PRODUCTION NAME</u></label>
                                                <select id="com_change" class="form-control" style="font-size: 11px" name="vessel_id" required>
                                                    <option value=""  >Select Name</option>
                                                    @foreach(\App\Vessel::all() as $v)
                                                        <option value="{{$v->id}}"  >{{$v->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <br>

                                        </div>
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="demo-select2-6" class="form-label" style="font-size: 11px"><u>FROM DATE</u></label>
                                                <input id="com_change" type="date" name="fromdate" class="form-control" style="font-size: 11px" required/>
                                            </div>
                                            <br>
                                            <div class="form-group">
                                                <label for="demo-select2-6" class="form-label" style="font-size: 11px"><u>TO DATE</u></label>
                                                <input id="com_change" type="date" name="todate" class="form-control" style="font-size: 11px" required/>
                                            </div>
                                            <br>

                                        </div>

                                        <input type="submit" class="btn btn-success form-control" value="View " style="font-size: 11px"/>


                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>

    @include('template.logoutView')

    @include('template.createuserView')

    @include('template.changepasswordview')




    <script src="assets/js/vendor.min.js"></script>
    <script src="assets/js/elephant.min.js"></script>
    <script src="assets/js/application.min.js"></script>
    <script src="assets/js/demo.min.js"></script>
    <script src="assets/parsley/js/parsley.min.js"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-83990101-1', 'auto');
        ga('send', 'pageview');
    </script>





</body>
</html>