<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>  DMT Crude Oil processing</title>
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta property="og:url" content="">
    <meta property="og:type" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:creator" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
    <link rel="apple-touch-icon" sizes="180x180" href="">
    <link rel="icon" type="image/png" href="favicon.ico" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon.ico" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#27ae60">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="assets/css/vendor.min.css">
    <link rel="stylesheet" href="assets/css/elephant.min.css">
    <link rel="stylesheet" href="assets/css/application.min.css">
    <link rel="stylesheet" href="assets/css/demo.min.css">
    <link rel="stylesheet" href="assets/parsley/css/parsley.css">

</head>
<body class="layout layout-header-fixed">
<div class="layout-header">
    @include('template.topbar')
</div>
<div class="layout-main">
    <div class="layout-sidebar" >
        <div class="layout-sidebar-backdrop" style="background-color: #217345;"></div>
        <div class="layout-sidebar-body" style="background-color: #217345;">
            <div class="custom-scrollbar" >
                <nav id="sidenav" class="sidenav-collapse collapse" style="background-color: #217345; color: #fff">
                    <ul class="sidenav">




                        <li class="sidenav-heading">Production Settings</li>

                        <li class="sidenav-item ">
                            <a href="vessel" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-truck"></span>
                                <span class="sidenav-label" style="font-size: 11px">Vessel</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="tank" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-beer"></span>
                                <span class="sidenav-label" style="font-size: 11px">Tank</span>
                            </a>

                        </li>

                        <li class="sidenav-item ">
                            <a href="product" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-group"></span>
                                <span class="sidenav-label" style="font-size: 11px">Refined Product</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="client" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-user"></span>
                                <span class="sidenav-label" style="font-size: 11px">Off Takers</span>
                            </a>

                        </li>




                        <li class="sidenav-heading">Depot Record</li>
                        <li class="sidenav-item ">
                            <a href="consignment" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">New Crude Oil </span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="refined" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">New Product</span>
                            </a>

                        </li>


                        <li class="sidenav-heading">Sale</li>
                        <li class="sidenav-item">
                            <a href="request" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-archive"></span>
                                <span class="sidenav-label" style="font-size: 11px">New Release</span>
                            </a>
                        </li>
                        <li class="sidenav-item">
                            <a href="load" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-suitcase"></span>
                                <span class="sidenav-label" style="font-size: 11px">Loading</span>
                            </a>
                        </li>
                        <li class="sidenav-heading">Report</li>
                        <li class="sidenav-item ">
                            <a href="report" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Summary Report</span>
                            </a>
                        </li>
                        <li class="sidenav-item ">
                            <a href="reportother" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Other Report</span>
                            </a>
                        </li>
                        <li class="sidenav-item">
                            <a href="remark" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Remarks</span>
                            </a>
                        </li>

                        <li class="sidenav-heading">User Management</li>

                        <li class="sidenav-item ">
                            <a href="#createUser" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-user"></span>
                                <span class="sidenav-label" style="font-size: 11px" data-toggle="modal" data-target="#createUser">Create New User</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="userutil" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-edit"></span>
                                <span class="sidenav-label" style="font-size: 11px" >User Utilities</span>
                            </a>

                        </li>

                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">

                <h1 class="title-bar-title">
                    <span class="d-ib">Depot Record </span>

                </h1>
                <p class="title-bar-description">
                    <small  style ="color:#217345">Utilities</small>

                </p>
            </div>


            <div class="pull-left">
                <button class=" btn  btn-success" style = "text-transform: capitalize; background-color: #217345" data-toggle="modal" data-target="#newDistrict" type="button"> <i class = "icon icon-plus-circle"></i> &nbsp New Record</button>


            </div>
            <br><br>
            <div class="row">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-actions">

                                <button type="button" class="card-action card-reload" title="Reload"></button>

                            </div>

                        </div>
                        <div class="card-body">

                            <table id="demo-datatables-responsive-1" class="table table-bordered table-striped table-nowrap dataTable" cellspacing="0" width="100%" style="font-size: 12px">
                                <thead>
                                <tr>
                                    <th>Record Day</th>
                                    <th>Vessel</th>
                                    <th>Tank </th>

                                    <th>quantity_production </th>

                                    <th>Action </th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php $x=1; ?>
                                @foreach($consignments as $p)

                                    <tr>
                                        <td>
                                            {{date("D M jS, Y", strtotime($p->name))}}
                                        </td>
                                        <td>
                                            {{\App\Vessel::find($p->vessel_id)->name}}
                                        </td>
                                        <td>
                                            {{\App\Tank::find($p->tank_id)->name}}
                                        </td>


                                        <td>
                                            {{number_format($p->quantity_processed,3)}}
                                        </td>

                                        <td>
                                            <center>
                                                <button class="deletebtn btn  btn-warning" style = "text-transform: capitalize;  background-color: maroon; ; border-color: maroon"  type="button" data-toggle="modal" data-target="#delDistrict"
                                                        data-id="{{$p->id}}"
                                                        data-name="{{$p->name}}"

                                                > <i class = "icon icon-trash"></i></button>
                                                <button class="editbtn btn  btn-warning" style = "text-transform: capitalize;  background-color: goldenrod; ; border-color: goldenrod"  type="button" data-toggle="modal" data-target="#editDistrict" data-id="{{$p->id}}" data-name="{{$p->name}}"
                                                        data-name="{{$p->name}}"
                                                        data-vessel_id="{{$p->vessel_id}}"
                                                        data-tank_id="{{$p->tank_id}}"
                                                        data-quantity_processed="{{$p->quantity_processed}}"

                                                > <i class = "icon icon-edit"></i></button>
                                            </center>
                                        </td>



                                    </tr>
                                @endforeach
                                <tfoot>
                                <tr>
                                    <td style="background-color:  #217345"></td>
                                    <td  style="background-color:  #217345"></td>
                                    <td style="background-color:  #217345; color: white;"><b>TOTAL</b></td>

                                    <td style="background-color:  #217345 ;color: white;"><b>{{number_format(\App\Consignment::all()->sum('quantity_processed'),3)}}</b></td>
                                    <td style="background-color:  #217345 ;color: white;"><b></b></td>

                                </tr>
                                </tfoot>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    @include('template.logoutView')

    @include('template.createuserView')

    @include('template.changepasswordview')

    <div id="delDistrict" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <form action="deleteconsignment" method="POST">
                {{csrf_field()}}
                <div class="modal-content">
                    <div class="modal-header bg-primary" style="background-color: #217345">
                        <button type="button" class="close" data-dismiss="modal" style="color: #fff">
                            <span aria-hidden="true" style="color: #fff">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <div class="text-center">
                            <span class="icon icon-trash icon-5x m-y-lg"></span>
                            <h4 class="modal-title" style="font-size: 14px">Depot record</h4>

                        </div>
                    </div>
                    <div class="modal-tabs">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#display" data-toggle="tab" style="font-size: 12px">Do You Want To Delete <span id="nameDelete"></span> district From System?</a></li>

                        </ul>
                        <input type="hidden" id="idDelete" name="idDelete"/>

                    </div>
                    <div class="modal-footer">
                        <center><button type="submit" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-check"></i>&nbsp &nbsp Yes &nbsp &nbsp</button>
                            <button type="button" class="btn btn-danger" style="background-color: maroon"><i class="icon icon-close"></i> Cancel</button></center>

                    </div>
                </div>
            </form>
        </div>
    </div>


    <div id="editDistrict" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <form action="updateconsignment" method="post" data-parsley-validate="" id="saveeupdate">
                {{csrf_field()}}
                <div class="modal-content">
                    <div class="modal-header bg-primary" style="background-color: #217345">
                        <button type="button" class="close" data-dismiss="modal" style="color: #fff" >
                            <span aria-hidden="true" style="color: #fff">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <div class="text-center">
                            <span class="icon icon-edit icon-5x m-y-lg"></span>
                            <h4 class="modal-title" style="font-size: 12px">Edit Depot Record Details</h4>

                        </div>
                    </div>
                    <div class="modal-tabs">

                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="display3">

                                <div class="form-group">
                                    <div class="row">
                                        <input type="hidden" id="idEdit" name="idEdit"/>

                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Name</label>
                                            <input id="nameEdit" name="nameEdit"  class="form-control" type="date" style="font-size: 11px" required="">
                                        </div>
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Vessel</label>
                                            <select id="vessel_idEdit" class="form-control" style="font-size: 11px" name="vessel_idEdit" required>
                                                <option value="" >Select Vessel</option>
                                                @foreach(App\Vessel::all() as $t)
                                                    <option value="{{$t->id}}" >{{$t->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4">
                                            <label  class="form-label" style="font-size: 12px">Crude oil Tank</label>
                                            <select id="tank_idEdit" class="form-control" style="font-size: 11px" name="tank_idEdit" required>
                                                <option value="" >Select Tank</option>
                                                @foreach(App\Tank::all()->where('purpose','1') as $t)
                                                    <option value="{{$t->id}}" >{{$t->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4">

                                            <label  class="form-label" style="font-size: 12px">quantity_processed</label>
                                            <input id="quantity_processedEdit" name="quantity_processedEdit"  class="form-control" type="number" min="0" value="0" step="0.001" style="font-size: 11px" required="">


                                        </div>
                                        <div class="col-md-4"></div>

                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">

                        <button  data-dismiss="modal" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-save"></i> Close</button>
                        <button type="submit" id="saveeupdate" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-save"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div id="newDistrict" class="modal fade "  tabindex="-1" role="dialog">
        <div class="modal-dialog" >
            <form action="saveconsignment" method="post" data-parsley-validate="" id="editform">
                {{csrf_field()}}
                <div class="modal-content">
                    <div class="modal-header bg-primary" style="background-color: #217345">
                        <button type="button" class="close" data-dismiss="modal" style="color: #fff" >
                            <span aria-hidden="true" style="color: #fff">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <div class="text-center">
                            <span class="icon icon-edit icon-5x m-y-lg"></span>
                            <h4 class="modal-title" style="font-size: 12px">New Depot Record Details</h4>

                        </div>
                    </div>
                    <div class="modal-tabs">

                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="display3">

                                <div class="form-group">
                                    <div class="row">
                                            <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Name</label>
                                            <input id="name" name="name"  class="form-control" value="{{old('name')}}" type="date" style="font-size: 11px" required="">
                                        </div>
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Vessel</label>
                                            <select id="vessel_id" class="form-control" style="font-size: 11px" name="vessel_id" required>
                                                <option value="" >Select Vessel</option>
                                                @foreach(App\Vessel::all() as $t)
                                                    <option value="{{$t->id}}" {{ old('vessel_id')==$t->id ? 'selected' : ''  }} >{{$t->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4">
                                            <label  class="form-label" style="font-size: 12px">Crude oil Tank</label>
                                            <select id="tank_id" class="form-control" style="font-size: 11px" name="tank_id" required>
                                                <option value="" >Select Tank</option>
                                                @foreach(App\Tank::all()->where('purpose','1') as $t)
                                                    <option value="{{$t->id}}" {{ old('tank_id')==$t->id ? 'selected' : ''  }} >{{$t->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4">

                                            <label  class="form-label" style="font-size: 12px">quantity_processed</label>
                                            <input id="quantity_processed" name="quantity_processed"  class="form-control" type="number" min="0" value="0" step="0.001" style="font-size: 11px" required="">


                                        </div>
                                        <div class="col-md-4"></div>

                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="submit" id="savee" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-save"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>



    <script src="assets/js/vendor.min.js"></script>
    <script src="assets/js/elephant.min.js"></script>
    <script src="assets/js/application.min.js"></script>
    <script src="assets/js/demo.min.js"></script>
    <script src="assets/parsley/js/parsley.min.js"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-83990101-1', 'auto');
        ga('send', 'pageview');
    </script>
    <script src="assets/js/toastr.min.js"></script>
<script>
    $(document).on('click','.editbtn',function(){
       // alert("ok");
        $('#nameEdit').val($(this).data('name'));
        $('#idEdit').val($(this).data('id'));
        $('#tank_idEdit').val($(this).data('tank_id')).select();
        $('#vessel_idEdit').val($(this).data('vessel_id')).select();
        $('#quantity_processedEdit').val($(this).data('quantity_processed')).select();
//        $('#gov_processedEdit').val($(this).data('gov_processed')).select();
    });
    $(document).on('click','.deletebtn',function() {
        $('#idDelete').val($(this).data('id'));
        $("#nameDelete").html($(this).data('name'));

    });
</script>


    <script>
        $(document).on('click','#viewby',function(){
            //
            var tank_id = $(this).data('tank_id');
            var date = $(this).data('date');

            $.ajax({

                type:"GET",
                url:"{{url('getByproductfortank')}}/"+date+"/"+tank_id,
                success: function(data) {

                    $('#databyproduct').empty();
                    var obj = JSON.parse(data);

                    for(var x in obj){


                        var strToAdd='<tr class="item">' +
                                '<td><div class="form-group">'+obj[x].product_name+' </div></td>' +
                                '<td><div class="form-group">'+obj[x].daily_measure+' </div></td>' +
                                '<td><div class="form-group">'+obj[x].daily_production+' </div></td>' +
                                '<td><div class="form-group">'+obj[x].cummulative_production+' </div> </td> ' +
                                '<td><div class="form-group">'+obj[x].daily_sale+' </div> </td> ' +
                                '<td><div class="form-group">'+obj[x].daily_balance+'</div> </td>' +
                                ' </tr>';
                        $('#databyproduct').append(strToAdd);
                    }
//
                }
            });
        });

    </script>
    <script>

        $(document).on('keyup','.daily_production',function(e) {
            var thisval=this.value;
            var row= parseInt(this.id.split("|")[1])- 1;
            var col1 =4;
            var col2 =6;
            var by_tank_id=$('.by_tank_id').val();
            var tank_id=$('#tank_id').val();
            $.ajax({
                type: 'POST',
                url: '{{URL::to('getcummulatedproduction')}}',
                data: {
                    '_token': "{{ csrf_token() }}",
                    'by_tank_id':by_tank_id,
                    'tank_id':tank_id
                },
                success: function (data) {

                    if(data == 0){

                        $('#data').find("tr").eq(row).find("td").eq(col1).find("input[type='text']").val(parseFloat(thisval).toFixed(3));
                        $('#data').find("tr").eq(row).find("td").eq(col2).find("input[type='text']").val(parseFloat(thisval).toFixed(2));
                    }else{
                        //alert(data);
                        $('#data').find("tr").eq(row).find("td").eq(col1).find("input[type='text']").val((parseFloat(thisval) +parseFloat(data)).toFixed(3));
                        $('#data').find("tr").eq(row).find("td").eq(col2).find("input[type='text']").val((parseFloat(thisval) + parseFloat(data)).toFixed(2));
                    }


                }
            })



        });
        $(document).on('keyup','#quantity_processed',function(e) {
            var content = $('#quantity_processed').val();
            var cummulative_determiner = $('#cummulative_determiner').val();
            var opening_determiner = $('#opening_determiner').val().split("|");

            if(cummulative_determiner != 0){
                $('#cummulative_processed').val(parseFloat(content) + parseFloat(cummulative_determiner));
            }else{
                $('#cummulative_processed').val(content);
            }

            if(opening_determiner[1] == 0){
                $('#opening_quantity').val(opening_determiner[0]);
            }else if(opening_determiner[1] == 1){
                $('#opening_quantity').val(parseFloat(opening_determiner[0])- parseFloat(content));
            }


        });
        $(document).on('change','#tank_id',function(e) {
            var tank_id=$(this).val();
            $.ajax({
                type: 'POST',
                url: '{{URL::to('getquantityreceived')}}',
                data: {
                    '_token': "{{ csrf_token() }}",
                    'tank_id':tank_id
                },
                success: function (data) {

                    //var obj = JSON.parse(data);
                    $('#opening_determiner').val(data);



                }
            })
            $.ajax({
                type: 'POST',
                url: '{{URL::to('consignmentstartedonTank')}}',
                data: {
                    '_token': "{{ csrf_token() }}",
                    'tank_id':tank_id
                },
                success: function (data) {
                    var obj = JSON.parse(data);

                    $('#cummulative_determiner').val(obj);

                }
            })
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            var currentItem = 0;
            var product;
            var bytankid;

            $.ajax({
                type:"GET",
                url:"{{url('productselectbox')}}",
                success: function(data) {
                    product=data;
                }
            });
            $.ajax({
                type:"GET",
                url:"{{url('bytankidselectbox')}}",
                success: function(data) {
                    bytankid=data;
                }
            });

            $('#addnew').click(function(){
                currentItem =currentItem+1;
                $('#number_of_items').val(currentItem);
                var strToAdd='<tr class="item">' +
                        '<td><div class="form-group"><select name="product_id[]" id="product_id|'+currentItem+'" class="form-control "  onchange="getval(this);" required>'+product+'</select> </div></td>' +
                        '<td><div class="form-group"><select name="by_tank_id[]" id="by_tank_id|'+currentItem+'" class="form-control by_tank_id" required readonly="">'+bytankid+'</select> </div></td>' +
                        '<td><div class="form-group"><input name="daily_measure[]" type="number" min="0" value="0" step="0.001" id="daily_measure|'+currentItem+'" class="form-control" required/> </div> </td> ' +
                        '<td><div class="form-group"> <input name="daily_production[]" type="number" min="0" value="0" step="0.001" id="daily_production|'+currentItem+'" class="form-control daily_production"  required /> </div> </td> ' +
                        '<td><div class="form-group"> <input name="cummulative_production[]" type="text"  id="cummulative_production|'+currentItem+'" class="form-control" readonly required  /> </div> </td>' +
                        '<td><div class="form-group"> <input name="daily_sale[]" type="number" min="0" value="0" step="0.001" id="daily_sale|'+currentItem+'" class="form-control" required  /> </div> </td>' +
                        '<td><div class="form-group"> <input name="daily_balance[]" type="text" id="daily_balance|'+currentItem+'" class="form-control" readonly required/> </div> </td>' +
                        '<td><button type="button" class="btn btn-primary remove" name="remove"  style="background-color: darkred"><span class="icon icon-minus-circle"></span></button> </td>'+
                        ' </tr>';
                $('#data').append(strToAdd);
            });
            $(document).on('click','.remove',function () {
                $(this).closest('tr').remove();
            })



        })
</script>
    <script>
        function getval(sel)
        {

            var row= parseInt(sel.id.split("|")[1])- 1;

            var  product_id=sel.value;
            $.ajax({
                type:"GET",
                url:"{{url('selecttankbyproductid')}}/"+product_id,
                success: function(data) {

                    $('#data').find("tr").eq(row).find("td").eq(1).find("select").val(data).change();

                }
            });
        }
    </script>
<script>
    $("#saveeupdate").submit(function(e){
        e.preventDefault();
        var formdata = $(this).serialize(); // here $(this) refere to the form its submitting

        $.ajax({
            type: 'POST',
            url: "{{ url('/updateconsignment') }}",
            data: formdata, // here $(this) refers to the ajax object not form
            success: function (data) {
                alert("Record Updated Successfully");
            },
        });

    });
</script>
</body>
</html>