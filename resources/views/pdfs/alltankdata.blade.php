<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>tank summary</title>
    <style>

        #datarecord,#heading {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            font-size: 10px;
        }
        #dated{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;

            font-size: 11px;
        }
        #datarecord td, #datarecord th {
            border: 1px solid black;
            padding: 8px;
        }

        #datarecord tr:nth-child(even){background-color: #f2f2f2;}

        #datarecord tr:hover {background-color: #ddd;}

        #datarecord th {
            padding-top: 9px;
            padding-bottom: 9px;
            text-align: left;
            background-color:  #217345;
            color: black;
            font-size: 10px;
        }
        div.page
        {
            page-break-after: always;
            page-break-inside: avoid;
        }
        #brieftable td {
            border-top: thin solid;
            border-bottom: thin solid;
        }

        #brieftable td:first-child {
            border-left: thin solid;
        }

        #brieftable td:last-child {
            border-right: thin solid;
        }
        #triangleimage { background: url("assets/img/tri.PNG") no-repeat; }

    </style>
</head>
<body>
<div>



    <br>
    <br>
    <img  src="assets/img/dmt.jpg" height="60px" alt="logo" />
    <br>

    <br>
    <span style="float: right; clear:both ; " id="dated">{{date("D M jS, Y", strtotime(date('Y-m-d')))}}</span>
    <br>

    <h1 id="heading" style="text-decoration: underline;"><center><b>{{\App\Vessel::find($vessel_id)->name}}</b></center></h1>

    <h3 id="heading"><center>RECORDS DETAILS CRUDE OIL IN  {{\App\Tank::find($tank_id)->name}} </center></h3>

    <table id="datarecord">
        <thead>
        <tr>
            <th rowspan="3" >DATE </th>
            <th  colspan="3"><center>TANK</center></th>

        </tr>
        <tr>

            <th colspan="3"><center>{{\App\Tank::find($tank_id)->name .'  Gov Quantity : '. \App\Tank::find($tank_id)->gov_quantity}}</center></th>


        </tr>
        <tr>
            <th >IN TANK ({{\App\Tank::find($tank_id)->quantity_in}}) </th>
            <th >QUANTITY PROCESSED</th>
            <th>CUMMULATIVE PROCESSED</th>

        </tr>
        </thead>
        <tbody>
        <?php $cummulative=0; $quantityin=0; ?>
        @foreach($tankdata as $d)
        <?php $cummulative=$cummulative + $d->quantity_processed ?>
        {{--\App\Tank::find($d->tank_id)->quantity_in - $cummulative--}}
        <tr>
           <td>{{date("D M jS, Y", strtotime($d->name))}}</td>
           <td>@if(sprintf("%.3f",\App\Tank::find($d->tank_id)->quantity_in) == sprintf("%.3f",$cummulative))
                   {{0}}
               @else
                   {{\App\Tank::find($d->tank_id)->quantity_in - $cummulative}}
               @endif
           </td>
           <td>{{$d->quantity_processed}}</td>
           <td>{{$cummulative }}</td>
        </tr>
        @endforeach
        <tbody>
        <tfoot>
        <tr>
            <td>Total</td>
            <td></td>
            <td>{{$cummulative}}</td>
            <td></td>
        </tr>

        </tfoot>

    </table>




</div>


</div>


</body>
</html>


