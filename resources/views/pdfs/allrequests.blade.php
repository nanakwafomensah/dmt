<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>tank summary</title>
    <style>

        #datarecord,#heading {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            font-size: 10px;
        }
        #dated{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;

            font-size: 11px;
        }
        #datarecord td, #datarecord th {
            border: 1px solid black;
            padding: 8px;
        }

        #datarecord tr:nth-child(even){background-color: #f2f2f2;}

        #datarecord tr:hover {background-color: #ddd;}

        #datarecord th {
            padding-top: 9px;
            padding-bottom: 9px;
            text-align: left;
            background-color:  #217345;
            color: black;
            font-size: 10px;
        }
        div.page
        {
            page-break-after: always;
            page-break-inside: avoid;
        }
        #brieftable td {
            border-top: thin solid;
            border-bottom: thin solid;
        }

        #brieftable td:first-child {
            border-left: thin solid;
        }

        #brieftable td:last-child {
            border-right: thin solid;
        }
        #triangleimage { background: url("assets/img/tri.PNG") no-repeat; }

    </style>
</head>
<body>
<div>



    <br>
    <br>
    <img  src="assets/img/dmt.jpg" height="60px" alt="logo" />
    <br>

    <br>
    <span style="float: right; clear:both ; " id="dated">{{date("D M jS, Y", strtotime(date('Y-m-d')))}}</span>
    <br>


    <h1 id="heading" style="text-decoration: underline;"><center><b>{{\App\Vessel::find($vessel_id)->name}}</b></center></h1>

    <h3 id="heading"><center>All REQUEST</center></h3>
    <table id="datarecord">
        <thead>
        <tr >
            <th >NUMBER </th>
            <th >REQUEST DATE</th>
            <th >CLIENT</th>
            <th >PRODUCT</th>
            <th >REFINED QUANTITY AT REQUEST</th>
            <th >REQUEST QUANTITY</th>
        </tr>
        </thead>
        <tbody>
<?php  $x=1; $sell_quantity=0;?>
        @foreach($requestss as $s)
            <?php $sell_quantity = $sell_quantity + $s->sell_quantity ?>
            <tr >
                <td>{{$x++}}</td>
                <td>{{date("D M jS, Y", strtotime($s->created_at))}}</td>
                <td>{{\App\Client::find($s->client_id)->name}}</td>
                <td>{{\App\Product::find($s->product_id)->name}}</td>
                <td>{{number_format($s->refined_quantity)}}</td>
                <td>{{number_format($s->sell_quantity)}}</td>
            </tr>
        @endforeach
        <tbody>
        <tfoot>
        <tr>
            <td><b>{{$requestss->count()}}</b></td>
            <td><b></b></td>
            <td><b></b></td>
            <td><b></b></td>
            <td><b>TOTAL</b></td>
            <td><b>{{number_format($sell_quantity)}}</b></td>
        </tr>
        </tfoot>

</table>




</div>


</div>


</body>
</html>


