<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>tank summary</title>
    <style>
        #datarecord,#heading {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            font-size: 10px;
        }

        #datarecord td, #datarecord th {
            border: 1px solid black;
            padding: 8px;
        }
        #dated{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;

            font-size: 11px;
        }
        #datarecord tr:nth-child(even){background-color: #f2f2f2;}

        #datarecord tr:hover {background-color: #ddd;}

        #datarecord th {
            padding-top: 9px;
            padding-bottom: 9px;
            text-align: left;
            background-color: white;
            border-color: black;
            font-size: 10px;
        }
        div.page
        {
            page-break-after: always;
            page-break-inside: avoid;
        }
        #brieftable td {
            border-top: thin solid;
            border-bottom: thin solid;
        }

        #brieftable td:first-child {
            border-left: thin solid;
        }

        #brieftable td:last-child {
            border-right: thin solid;
        }

    </style>
</head>
<body>
<div>

    <div class="page">
        <h4 id="heading">
            <center><b>TANK SUMMARY REPORT</b></center>
        </h4>
        <h1 id="heading" style="text-decoration: underline;"><center><b>{{\App\Vessel::find($vessel_id)->name}}</b></center></h1>
        <br>
        <img  src="assets/img/dmt.jpg"  height="60px" alt="logo" />
        <br>
         <table id="datarecord">
             <tr style="line-height: 1px;">
                 <th style='text-align:center;vertical-align:middle;width: 50%;background-color: #f2f2f2;'>DMT COLLATERAL LTD<br>
                     <p>H/N F4/1C,28 February Road,Castle Junction Osu-Accra</p>
                 </th>
                  <th style='text-align:center;vertical-align:middle;width: 50%;background-color: #217345;'>
                      TEMA OIL REFINERY(TOR)
                  </th>

             </tr>
         </table>
        {{--<br>--}}
        {{--<img  src="assets/img/dmt.jpg"  height="60px" alt="logo" />--}}
        {{--<br>--}}

        <br>
        <span style="float: right; clear:both ; " id="dated">{{date("D M jS, Y", strtotime($date))}}</span>
        <br>
        <br>


        <h3 id="heading"><center>CRUDE OIL TANKS SUMMARY(METRIC TONS IN AIR) </center></h3>
        <table id="datarecord">
            <thead>

            <tr>
                <th rowspan="2" style="background-color: #217345;color:black; ">TANK/QUANTITY </th>

                @foreach($quantity_in_tank as $item)
                <th style="background-color: #217345;color:black; " colspan="2" ><center><b>{{$item->name}}</b></center></th>
                @endforeach
                <th style="background-color: #217345;color:black;" colspan="2"><center><b>TOTAL</b></center></th>
            </tr>
            <tr>

                @foreach($quantity_in_tank as $item)
                    <td style="background-color: #217345;color:black; " ><center><b>TONNES /AIR</b></center></td>
                    <td style="background-color: #217345;color:black; " ><center><b>GOV</b></center></td>
                @endforeach
                <td style="background-color: #217345;color:black; "><b>TONNES /AIR</b></td>
                <td style="background-color: #217345;color:black; "><b>GOV</b></td>


            </tr>
            </thead>
            <tbody>

               <tr>
                    <td >QUANTITY IN TANK</td>
                   @foreach($quantity_in_tank as $item)
                       <td >{{number_format($item->quantity_in,3)}} </td>
                       <td >{{number_format($item->gov_quantity,3)}} </td>
                   @endforeach
                   <td>{{number_format(\App\Tank::where('vessel_id',$vessel_id)->sum('quantity_in'),3)}}</td>
                   <td>{{number_format(\App\Tank::where('vessel_id',$vessel_id)->sum('gov_quantity'),3)}}</td>


                </tr>

                <tr>
                    <td >QUANTITY PROCESSED</td>

                    @foreach($quantity_in_tank as $item)
                    <td>{{number_format(\App\Consignment::where('tank_id',$item->id)->where('vessel_id',$vessel_id)->sum('quantity_processed'),3)}}</td>
                    <td>-</td>

                    @endforeach

                    <td>{{number_format(\App\Consignment::where('vessel_id',$vessel_id)->sum('quantity_processed'),3)}}</td>
                    <td>-</td>


                </tr>

                <tr>
                    <td >OPERATIONAL LOSS</td>

                    @foreach($quantity_in_tank as $item)
                    <td>{{number_format(\App\Tank::find($item->id)->where('vessel_id',$vessel_id)->sum('opcost'),3)}}</td>
                    <td>-</td>

                    @endforeach
                    <td>{{number_format(\App\Tank::find($vessel_id)->where('vessel_id',$vessel_id)->sum('opcost'),3)}}</td>
                    <td>-</td>


                </tr>

                <tr>
                    <td >REMAINING</td>

                    @foreach($quantity_in_tank as $item)

                        <td >{{number_format(\App\Helpers\AppHelper::getremaining($item->id,$vessel_id),3)}}</td>
                        <td >-</td>

                    @endforeach
                    <td>{{number_format(\App\Helpers\AppHelper::totalremain($vessel_id),3)}}</td>
                    <td>-</td>



                </tr>

            <tbody>

        </table>
<br>


    </div>
<div class="page">
    <h4 id="heading">
        <center><b>TANK SUMMARY REPORT</b></center>


    </h4>
    <br>
    <img  src="assets/img/dmt.jpg"  height="60px" alt="logo" />
    <br>
    <table id="datarecord">
        <tr style="line-height: 1px;">
            <th style='text-align:center;vertical-align:middle;width: 50%;background-color: #f2f2f2;'>DMT COLLATERAL LTD<br>
                <p>H/N F4/1C,28 February Road,Castle Junction Osu-Accra</p>
            </th>
            <th style='text-align:center;vertical-align:middle;width: 50%;background-color: #217345;'>
                TEMA OIL REFINERY(TOR)
            </th>

        </tr>
    </table>


    <br>
    <span style="float: right; clear:both ; " id="dated">{{date("D M jS, Y", strtotime($date))}}</span>
    <br>
    <br>

    <h3 id="heading"><center>REFINED PRODUCTS TANKS (METRIC TONS IN AIR)</center></h3>
    <table id="datarecord">

        <thead>
        <tr>
            <th rowspan="2" style="background-color: #217345;color:black;" ><center>TANKS</center> </th>
            <th rowspan="2" style="background-color: #217345;color:black;"><center>PRODUCT</center></th>
            <th colspan="2" style="background-color: #217345;color:black;"><center>QUANTITY</center></th>
        </tr>
        </thead>
        <tbody>
        <tr>

            <td style="background-color: #217345;"><center><b>TONNES AIR</b></center></td>
            <td style="background-color: #217345;"><center><b>GOV</b></center></td>

        </tr>
        @foreach($byproduct as $y)
            <tr>
                <td><center>{{$y->name }}</center></td>
                <td><center>{{\App\Product::where('tank_id',$y->id)->pluck('name')->first()}}</center> </td>
                <td><center>{{number_format(\App\Helpers\AppHelper::summarydailyproduction($y->id),3)}}</center></td>
                <td><center>{{number_format(\App\Helpers\AppHelper::summarygovproduction($y->id),3)}}</center></td>
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td><center><b>TOTAL</b></center></td>
            <td><center><b>{{number_format(\App\Helpers\AppHelper::summarydailyproductiontotal(),3)}}</b></center></td>
            <td><center><b>{{number_format(\App\Helpers\AppHelper::summarygovproductiontotal(),3)}}</b></center></td>
        </tr>
        <tbody>

    </table>
<br>
    <div class="form-group">
        <label for="comment" id="heading">Remark:{{$remark}}</label>
        <input class="form-control"  id="comment" value=""/>
    </div>

    </div>


    </div>


</div>


</body>
</html>