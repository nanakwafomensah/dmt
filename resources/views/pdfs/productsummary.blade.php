<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>tank summary</title>
    <style>
        #datarecord,#heading {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            font-size: 10px;
        }
        #dated{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;

            font-size: 11px;
        }
        #datarecord td, #datarecord th {
            border: 1px solid black;
            padding: 8px;
        }

        #datarecord tr:nth-child(even){background-color: #f2f2f2;}

        #datarecord tr:hover {background-color: #ddd;}

        #datarecord th {
            padding-top: 9px;
            padding-bottom: 9px;
            text-align: left;
            background-color:  black;
            color: black;
            font-size: 10px;
        }
        div.page
        {
            page-break-after: always;
            page-break-inside: avoid;
        }
        #brieftable td {
            border-top: thin solid;
            border-bottom: thin solid;
        }

        #brieftable td:first-child {
            border-left: thin solid;
        }

        #brieftable td:last-child {
            border-right: thin solid;
        }
        #triangleimage { background: url("assets/img/tri.PNG") no-repeat; }

    </style>
</head>
<body>
<div>


    <h4 id="heading">
        <center><b>PRODUCT SUMMARY REPORT</b></center>
    </h4>
    <h1 id="heading" style="text-decoration: underline;"><center><b>{{\App\Vessel::find($vessel_id)->name}}</b></center></h1>

    <br>
    <img  src="assets/img/dmt.jpg" height="60px" alt="logo" />
    <br>
    <table id="datarecord">
        <tr style="line-height: 1px;">
            <th style='text-align:center;vertical-align:middle;width: 50%;background-color: #f2f2f2;'>DMT COLLATERAL LTD<br>
                <p>H/N F4/1C,28 February Road,Castle Junction Osu-Accra</p>
            </th>
            <th style='text-align:center;vertical-align:middle;width: 50%;background-color: #217345;'>
                TEMA OIL REFINERY(TOR)
            </th>

        </tr>
    </table>



    <br>
    <span style="float: right; clear:both ; " id="dated">{{date("D M jS, Y", strtotime($date))}}</span>
    <br>
    <br>

    

        <h3 id="heading"><center>PRODUCT SUMMARY REPORT(LITRES)</center></h3>
        <table id="datarecord">

            <thead>
            <tr style="">
                <th style="background-color:  #217345;color: black; "   >PRODUCT </th>
                {{--<th style="background-color:  #217345;color: black; "   >REFINED QUANTITY(AIR) </th>--}}
                <th style="background-color:  #217345;color: black; "   >REFINED QUANTITY</th>



                <th style="background-color:  #217345;color: black; "   >QUANTITY SOLD </th>

                <th style="background-color:  #217345;color: black; "  >QUANTITY LIFTED </th>

                <th style="background-color:  #217345;color: black; "   >REMAINING AFTER SALES </th>

                <th style="background-color:  #217345;color: black; "   >REMAINING AFTER LIFTING </th>

            </tr>

            </thead>
            <tbody>

@foreach($products as $p)
                <tr >
                    <td ><center>{{$p->name}}</center></td>
{{--                    <td>{{ number_format(\App\Helpers\AppHelper::productsummary_refined($p->id,$vessel_id))}}</td>--}}
                    <td>{{ number_format(\App\Helpers\AppHelper::productsummarygov_refined($p->id,$vessel_id)) }}</td>



                    <td>{{number_format(\App\Helpers\AppHelper::productsummary_sold($p->id,$vessel_id))}}</td>

                    <td>{{number_format(\App\Helpers\AppHelper::productsummary_lifted($p->id,$vessel_id))}}</td>

                    <td>{{number_format(\App\Helpers\AppHelper::productsummary_remainaftersales($p->id,$vessel_id))}}</td>

                    <td>{{number_format(\App\Helpers\AppHelper::productsummary_remainafterlift($p->id,$vessel_id))}}</td>
                </tr>

@endforeach

            <tbody>
            <tfoot>
            <tr  >
                <td style=""><center><b>TOTAL</b></center></td>
                {{--<td><b>{{number_format(\App\Helpers\AppHelper::total_summaryrefined($vessel_id))}}</b></td>--}}
                <td><b>{{number_format(\App\Helpers\AppHelper::total_summarygovrefined($vessel_id))}}</b></td>



                <td><b>{{number_format(\App\Helpers\AppHelper::total_summarysold($vessel_id))}}</b></td>

                <td><b>{{number_format(\App\Helpers\AppHelper::total_summarylifted($vessel_id))}}</b></td>

                <td><b>{{number_format(\App\Helpers\AppHelper::total_remainaftersales($vessel_id))}}</b></td>

                <td><b>{{number_format(\App\Helpers\AppHelper::total_remainafterlift($vessel_id))}}</b></td>
            </tr>
            </tfoot>
        </table>
   <br>
    <div class="form-group">
        <label for="comment" id="heading">Remark:{{$remark}}</label>
        <input class="form-control"  id="comment" value=""/>
    </div>




</div>


</div>


</body>
</html>