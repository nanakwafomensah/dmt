<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>salestock</title>
    <style>
        #datarecord,#heading {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            font-size: 10px;
        }
 #dated{
     font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;

     font-size: 11px;
 }
        #datarecord td, #datarecord th {
            border: 1px solid black;
            padding: 10px;
        }

        #datarecord tr:nth-child(even){background-color: #f2f2f2;}

        #datarecord tr:hover {background-color: #ddd;}

        #datarecord th {
            padding-top: 9px;
            padding-bottom: 9px;
            text-align: left;
            /*background-color: white;*/
            color: black;
            font-size: 6px;
        }
        div.page
        {
            page-break-after: always;
            page-break-inside: avoid;
        }
        #brieftable td {
            border-top: thin solid;
            border-bottom: thin solid;
        }

        #brieftable td:first-child {
            border-left: thin solid;
        }

        #brieftable td:last-child {
            border-right: thin solid;
        }
        .products{
            text-align: center;
            background-color: #217345;
        }
        .purchaser{

            background-color: #217345;
        }
    </style>
</head>
<body>
<div>



    <h4 id="heading">
        <center><b>SALES STOCK REPORT</b></center>

    </h4>
    <h1 id="heading" style="text-decoration: underline;"><center><b>{{\App\Vessel::find($vessel_id)->name}}</b></center></h1>

    <br>
    <img  src="assets/img/dmt.jpg" height="60px" alt="logo" />
    </br>
    <table id="datarecord">
        <tr style="line-height: 1px;">
            <th style='text-align:center;vertical-align:middle;width: 50%;background-color: #f2f2f2;'>DMT COLLATERAL LTD<br>
                <p>H/N F4/1C,28 February Road,Castle Junction Osu-Accra</p>
            </th>
            <th style='text-align:center;vertical-align:middle;width: 50%;background-color: #217345;'>
                TEMA OIL REFINERY(TOR)
            </th>

        </tr>
    </table>


<br>

        <span style="float: right; clear:both ; " id="dated">{{date("D M jS, Y", strtotime($date))}}</span>
<br>
<br>


        <center><h3 id="heading">SALES SUMMARY OF REFINED PRODUCTS IN LITERS</h3> </center>
    <div class="page">
        <table  id="datarecord">

                <thead>

                <tr>
                    <th rowspan="3" class="purchaser">PURCHASERS</th>
                    <th colspan="{{count($x)* 3}}" class="products"><center>PRODUCT(S) IN LITERS</center></th>
                </tr>
                <tr>
                    @for($count=0;$count < count($x); $count++)
                        @if($x[$count] === 3 || $x[$count] === 5 || $x[$count] === 4 )
                    <th colspan="3" class="products"><center>{{\App\Product::find($x[$count])->name}}</center></th>
                        @endif
                    @endfor
                </tr>
                <tr>
                    @for($count=0;$count < count($x); $count++)
                        @if($x[$count] <=9)
                        <th class="purchaser">Rel</th>
                        <th class="purchaser">Lif</th>
                        <th class="purchaser">Rem</th>
                        @endif
                    @endfor

                </tr>
                </thead>
               <tbody>
               @foreach($clientrequest as $child)
                   <tr>
                       <?php $i = 0; $len = count($child);?>
                   @foreach($child as $val)
                               @if ($i == 0) {
                               <td >{{$val}}</td>
                               @else
                               <td>{{number_format(explode("|",$val)[0])}}</td>
                               <td>{{number_format(explode("|",$val)[1])}}</td>
                               <td>{{number_format(explode("|",$val)[2])}}</td>
                               @endif
                              <?php $i++; ?>
                    @endforeach
                   </tr>
                   @endforeach
                </tr>
                   <tr>
                       <th >TOTAL</th>
                       @for($count=0;$count < count($totals);$count++)
                           <td style="font-weight: bold;">{{number_format(explode("|",$totals[$count])[0])}}</td>
                           <td style="font-weight: bold;">{{number_format(explode("|",$totals[$count])[1])}}</td>
                           <td style="font-weight: bold;">{{number_format(explode("|",$totals[$count])[2])}}</td>
                       @endfor
                   </tr>
                </tbody>
            </table>
        <br>
        <div class="form-group">
            <label for="comment" id="heading">Remark:{{$remark}}</label>
            <input class="form-control"  id="comment" value=""/>
        </div>

        <div id="heading">
            <b>Rel : </b>Release &nbsp;<b>Lif : </b>Lifting &nbsp;<b>Rem : </b>Remainder
        </div>
    </div>


    <div class="page">
            <table  id="datarecord">

        <thead>

        <tr>
            <th rowspan="3" class="purchaser">PURCHASERS</th>
            <th colspan="{{count($x)* 3}}" class="products"><center>PRODUCT(S) IN LITERS</center></th>
        </tr>
        <tr>
            @for($count=0;$count < count($x); $count++)
                @if($x[$count] > 9)
                    <th colspan="3" class="products"><center>{{\App\Product::find($x[$count])->name}}</center></th>
                @endif
            @endfor
        </tr>
        <tr>
            @for($count=0;$count < count($x); $count++)
                @if($x[$count] > 9)
                    <th class="purchaser">Rel</th>
                    <th class="purchaser">Lif</th>
                    <th class="purchaser">Rem</th>
                @endif
            @endfor

        </tr>
        </thead>
        <tbody>
        @foreach($clientrequest2 as $child)
            <tr>
                <?php $i = 0; $len = count($child);?>
                @foreach($child as $val)
                    @if ($i == 0) {
                    <td >{{$val}}</td>
                    @else
                        <td>{{number_format(explode("|",$val)[0])}}</td>
                        <td>{{number_format(explode("|",$val)[1])}}</td>
                        <td>{{number_format(explode("|",$val)[2])}}</td>
                    @endif
                    <?php $i++; ?>
                @endforeach
            </tr>
            @endforeach
            </tr>
            <tr>
                <th >TOTAL</th>
                @for($count=0;$count < count($totals2);$count++)
                    <td style="font-weight: bold;">{{number_format(explode("|",$totals2[$count])[0])}}</td>
                    <td style="font-weight: bold;">{{number_format(explode("|",$totals2[$count])[1])}}</td>
                    <td style="font-weight: bold;">{{number_format(explode("|",$totals2[$count])[2])}}</td>
                @endfor
            </tr>
        </tbody>
    </table>
        <div class="form-group">
            <label for="comment" id="heading">Remark:{{$remark}}</label>
            <input class="form-control"  id="comment" value=""/>
        </div>
        <div id="heading">
            <b>Rel : </b>Release &nbsp;<b>Lif : </b>Lifting &nbsp;<b>Rem : </b>Remainder
        </div>
       </div>









</div>


</div>


</body>
</html>