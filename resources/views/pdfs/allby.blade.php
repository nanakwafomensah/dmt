<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>tank summary</title>
    <style>

        #datarecord,#heading {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            font-size: 10px;
        }
        #dated{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;

            font-size: 11px;
        }
        #datarecord td, #datarecord th {
            border: 1px solid black;
            padding: 8px;
        }

        #datarecord tr:nth-child(even){background-color: #f2f2f2;}

        #datarecord tr:hover {background-color: #ddd;}

        #datarecord th {
            padding-top: 9px;
            padding-bottom: 9px;
            text-align: left;
            background-color:  #217345;
            color: black;
            font-size: 10px;
        }
        div.page
        {
            page-break-after: always;
            page-break-inside: avoid;
        }
        #brieftable td {
            border-top: thin solid;
            border-bottom: thin solid;
        }

        #brieftable td:first-child {
            border-left: thin solid;
        }

        #brieftable td:last-child {
            border-right: thin solid;
        }
        #triangleimage { background: url("assets/img/tri.PNG") no-repeat; }

    </style>
</head>
<body>
<div>



    <br>
    <br>
    <img  src="assets/img/dmt.jpg" height="60px" alt="logo" />
    <br>

    <br>
    <span style="float: right; clear:both ; " id="dated">{{date("D M jS, Y", strtotime(date('Y-m-d')))}}</span>
    <br>


    <h1 id="heading" style="text-decoration: underline;"><center><b>{{\App\Vessel::find($vessel_id)->name}}</b></center></h1>

    <h3 id="heading"><center>DETAILED  {{\App\Product::find($product_id)->name}} BY-PRODUCT RECORDS</center></h3>
    <table id="datarecord">
        <thead>
        <tr>
            <th rowspan="4" >DATE </th>
            <th  colspan="8"><center>BY-PRODUCT / REFINED</center></th>

        </tr>
        <tr>

            <th colspan="8"><center>{{\App\Product::find($product_id)->name}}</center></th>


        </tr>
        <tr>

            <th ><center>DAILY MEASURE</center></th>
            <th colspan="2"><center>DAILY PRODUCTION</center></th>
            <th colspan="2"><center>CUMMULATED PRODUCTION</center></th>
            <th><center>DAILY SALES</center></th>

            <th colspan="2"><center>DAILY BALANCE</center></th>

        </tr>
        <tr>
            <th>metric tons/air</th>
            <th>gov</th>
            <th>metric tons/air</th>
            <th>gov</th>
            <th>metric tons/air</th>
            <th>metric tons/air</th>
            <th>metric tons/air</th>
            <th>gov</th>




        </tr>
        </thead>
        <tbody>
        <?php $daily_measure=0; $daily_productiom=0; $gov_quantity=0; $daily_sale=0; $cummulated_production=0;$cummulated_gov=0; ?>
        @foreach($productdata as $d)
            <?php
            $daily_measure = $daily_measure + $d->daily_measure;
            $daily_sale = $daily_sale + $d->daily_sale;
            $daily_productiom = $daily_productiom + $d->daily_production;
            $gov_quantity = $gov_quantity + $d->gov_quantity;
            $cummulated_production =$cummulated_production + $d->daily_production;
            $cummulated_gov =$cummulated_gov + $d->gov_quantity;
            ?>
            <tr>
                <td>{{date("D M jS, Y", strtotime($d->name))}}</td>
                <td>{{$d->daily_measure}}</td>
                <td>{{$d->gov_quantity}}</td>
                <td>{{$d->daily_production}}</td>
                <td>{{$cummulated_gov}}</td>
                <td>{{$cummulated_production}}</td>
                <td>{{$d->daily_sale}}</td>
                <td>{{ round($cummulated_production, 2) }}</td>
                <td>{{ round($cummulated_gov, 2) }}</td>
            </tr>
        @endforeach
        <tbody>
        <tfoot>
        <tr >
            <td>Total</td>
            <td>{{$daily_measure}}</td>
            <td>{{$gov_quantity}}</td>
            <td>{{$daily_productiom}}</td>
            <td></td>
            <td></td>
            <td>{{$daily_sale}}</td>
            <td></td>
            <td></td>
        </tr>

        </tfoot>

    </table>




</div>


</div>


</body>
</html>


