<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>  DMT Crude Oil processing</title>
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta property="og:url" content="">
    <meta property="og:type" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:creator" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
    <link rel="apple-touch-icon" sizes="180x180" href="">
    <link rel="icon" type="image/png" href="favicon.ico" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon.ico" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#27ae60">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="assets/css/vendor.min.css">
    <link rel="stylesheet" href="assets/css/elephant.min.css">
    <link rel="stylesheet" href="assets/css/application.min.css">
    <link rel="stylesheet" href="assets/css/demo.min.css">
    <link rel="stylesheet" href="assets/parsley/css/parsley.css">

</head>
<body class="layout layout-header-fixed">
<div class="layout-header">
    @include('template.topbar')
</div>
<div class="layout-main">
    <div class="layout-sidebar" >
        <div class="layout-sidebar-backdrop" style="background-color: #217345;"></div>
        <div class="layout-sidebar-body" style="background-color: #217345;">
            <div class="custom-scrollbar" >
                <nav id="sidenav" class="sidenav-collapse collapse" style="background-color: #217345; color: #fff">
                    <ul class="sidenav">
                        <li class="sidenav-heading">Production Setting</li>
                        <li class="sidenav-item ">
                            <a href="vessel" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-truck"></span>
                                <span class="sidenav-label" style="font-size: 11px">Vessel</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="tank" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-beer"></span>
                                <span class="sidenav-label" style="font-size: 11px">Tank</span>
                            </a>

                        </li>

                        <li class="sidenav-item ">
                            <a href="product" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-group"></span>
                                <span class="sidenav-label" style="font-size: 11px">Refined Product</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="client" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-user"></span>
                                <span class="sidenav-label" style="font-size: 11px">Off Takers</span>
                            </a>

                        </li>




                        <li class="sidenav-heading">Depot Record</li>
                        <li class="sidenav-item ">
                            <a href="consignment" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">New Crude Oil </span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="refined" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">New Product</span>
                            </a>

                        </li>

                        <li class="sidenav-heading">Sale</li>
                        <li class="sidenav-item active">
                            <a href="request" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-archive"></span>
                                <span class="sidenav-label" style="font-size: 11px">New Release</span>
                            </a>
                        </li>
                        <li class="sidenav-item">
                            <a href="load" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-suitcase"></span>
                                <span class="sidenav-label" style="font-size: 11px">Loading</span>
                            </a>
                        </li>
                        <li class="sidenav-heading">Report</li>
                        <li class="sidenav-item ">
                            <a href="report" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Summary Report</span>
                            </a>
                        </li>
                        <li class="sidenav-item ">
                            <a href="reportother" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Other Report</span>
                            </a>
                        </li>
                        <li class="sidenav-item">
                            <a href="remark" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Remarks</span>
                            </a>
                        </li>

                        <li class="sidenav-heading">User Management</li>

                        <li class="sidenav-item ">
                            <a href="#createUser" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-user"></span>
                                <span class="sidenav-label" style="font-size: 11px" data-toggle="modal" data-target="#createUser">Create New User</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="userutil" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-edit"></span>
                                <span class="sidenav-label" style="font-size: 11px" >User Utilities</span>
                            </a>

                        </li>

                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">

                <h1 class="title-bar-title">
                    <span class="d-ib">Release </span>

                </h1>
                <p class="title-bar-description">
                    <small  style ="color:#217345">Utilities</small>

                </p>
            </div>


            <div class="pull-left">
                <button class=" btn  btn-success" style = "text-transform: capitalize; background-color: #217345" data-toggle="modal" data-target="#newDistrict" type="button"> <i class = "icon icon-plus-circle"></i> &nbsp New Request</button>


            </div>
            <br><br>
            <div class="row">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-actions">

                                <button type="button" class="card-action card-reload" title="Reload"></button>

                            </div>

                        </div>
                        <div class="card-body">

                            <table id="demo-datatables-responsive-1" class="table table-bordered table-striped table-nowrap dataTable" cellspacing="0" width="100%" style="font-size: 12px">
                                <thead>
                                <tr>
                                    <th> Number</th>
                                    <th> Date</th>
                                    <th> Purchaser</th>
                                    <th> Product</th>

                                    <th> Refined Quantity at Sales (litres)</th>
                                    <th> Quantity Request</th>
                                    <th><center>Action</center></th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php $x=1; ?>
                                @foreach($requests as $p)

                                    <tr>
                                        <td>
                                            {{$x++}}
                                        </td>
                                       <td>
                                            {{date("D M jS, Y", strtotime($p->request_date))}}
                                        </td>
                                        <td>
                                            {{\App\Client::find($p->client_id)->name}}
                                        </td>
                                        <td>
                                            {{\App\Product::find($p->product_id)->name}}
                                        </td>

                                        <td>
                                            {{$p->refined_quantity}}
                                        </td>
                                         <td>
                                            {{$p->sell_quantity}}
                                        </td>


                                        <td>
                                            <center>
                                                <button class="deletebtn btn  btn-warning" style = "text-transform: capitalize;  background-color: maroon; ; border-color: maroon"  type="button" data-toggle="modal" data-target="#delDistrict"
                                                        data-id="{{$p->id}}"
                                                        data-client_id="{{\App\Client::find($p->client_id)->name}}"

                                                > <i class = "icon icon-trash"></i></button>
                                                <button class="editbtn btn  btn-warning" style = "text-transform: capitalize;  background-color: goldenrod; ; border-color: goldenrod"  type="button" data-toggle="modal" data-target="#editDistrict" data-id="{{$p->id}}" data-name="{{$p->name}}"
                                                        data-client_id="{{$p->client_id}}"
                                                        data-product_id="{{$p->product_id}}"
                                                        data-vessel_id="{{$p->vessel_id}}"
                                                        data-refined_quantity="{{$p->refined_quantity}}"
                                                        data-sell_quantity="{{$p->sell_quantity}}"
                                                        data-remain_quantity="{{$p->remain_quantity}}"
                                                        data-request_date="{{$p->request_date}}"

                                                > <i class = "icon icon-edit"></i></button>
                                            </center>
                                        </td>

                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    @include('template.logoutView')

    @include('template.createuserView')

    @include('template.changepasswordview')

    <div id="delDistrict" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <form action="deleterequest" method="POST">
                {{csrf_field()}}
                <div class="modal-content">
                    <div class="modal-header bg-primary" style="background-color: #217345">
                        <button type="button" class="close" data-dismiss="modal" style="color: #fff">
                            <span aria-hidden="true" style="color: #fff">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <div class="text-center">
                            <span class="icon icon-trash icon-5x m-y-lg"></span>
                            <h4 class="modal-title" style="font-size: 14px">DeleteRequest</h4>

                        </div>
                    </div>
                    <div class="modal-tabs">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#display" data-toggle="tab" style="font-size: 12px">Do You Want To Delete <span id="nameDelete"></span> Request From System?</a></li>

                        </ul>
                        <input type="hidden" id="idDelete" name="idDelete"/>

                    </div>
                    <div class="modal-footer">
                        <center><button type="submit" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-check"></i>&nbsp &nbsp Yes &nbsp &nbsp</button>
                            <button type="button" class="btn btn-danger" style="background-color: maroon"><i class="icon icon-close"></i> Cancel</button></center>

                    </div>
                </div>
            </form>
        </div>
    </div>


    <div id="editDistrict" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <form action="updaterequest" method="post" data-parsley-validate="" id="updaterequest">
                {{csrf_field()}}
                <div class="modal-content">
                    <div class="modal-header bg-primary" style="background-color: #217345">
                        <button type="button" class="close" data-dismiss="modal" style="color: #fff" >
                            <span aria-hidden="true" style="color: #fff">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <div class="text-center">
                            <span class="icon icon-edit icon-5x m-y-lg"></span>
                            <h4 class="modal-title" style="font-size: 12px">Edit Request Details</h4>

                        </div>
                    </div>
                    <div class="modal-tabs">

                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="display3">

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label  class="form-label" style="font-size: 12px">Date</label>
                                            <input id="request_dateEdit" type="date" class="form-control" style="font-size: 11px" name="request_dateEdit" required />

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label  class="form-label" style="font-size: 12px">Select Vessel</label>
                                            <select id="vessel_idEdit" class="form-control" style="font-size: 11px" name="vessel_idEdit" required>
                                                <option value="" >Select Vessel</option>
                                                @foreach(\App\Vessel::all() as $v)
                                                    <option value="{{$v->id}}" >{{$v->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <input type="hidden" id="idEdit" name="idEdit"/>
                                        <div class="col-md-12">
                                            <label  class="form-label" style="font-size: 12px">client Name</label>
                                            <select id="client_idEdit" class="form-control" style="font-size: 11px" name="client_idEdit">
                                                <option value="" >Select Tank</option>
                                                @foreach(\App\Client::all() as $s)
                                                    <option value="{{$s->id}}" >{{$s->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                     </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label  class="form-label" style="font-size: 12px">Product Name</label>
                                            <select id="product_idEdit" class="form-control" style="font-size: 11px" name="product_idEdit">
                                                <option value="" >Select Tank</option>
                                                @foreach(\App\Product::all() as $s)
                                                    <option value="{{$s->id}}" >{{$s->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                    <div class="row">

                                        <div class="col-md-12">
                                            <label  class="form-label" style="font-size: 12px">Quantity Refined</label>
                                            <input readonly id="refined_quantityEdit" name="refined_quantityEdit"  class="form-control" type="number"  min="0" value="0" step="0.001" style="font-size: 11px" required="" >
                                        </div>

                                    </div>
                                    <div class="row">

                                        <div class="col-md-12">

                                            <label  class="form-label" style="font-size: 12px">Remaining Quantity</label>

                                            <input readonly id="remain_quantityEdit" name="remain_quantityEdit"  class="form-control" type="number" min="0" value="0" step="0.001" style="font-size: 11px" required="" >

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label  class="form-label" style="font-size: 12px">Sell Quantity</label>
                                            <input id="sell_quantityEdit" name="sell_quantityEdit"  class="form-control" type="number" min="0" value="0" step="0.001" style="font-size: 11px" required="" >


                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="submit" id="savee" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-save"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="newDistrict" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <form action="saverequest" method="post" data-parsley-validate="" id="editform">
                {{csrf_field()}}
                <div class="modal-content">
                    <div class="modal-header bg-primary" style="background-color: #217345">
                        <button type="button" class="close" data-dismiss="modal" style="color: #fff" >
                            <span aria-hidden="true" style="color: #fff">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <div class="text-center">
                            <span class="icon icon-edit icon-5x m-y-lg"></span>
                            <h4 class="modal-title" style="font-size: 12px">Add Request </h4>

                        </div>
                    </div>
                    <div class="modal-tabs">

                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="display3">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label  class="form-label" style="font-size: 12px">Date</label>
                                            <input id="request_date" class="form-control" value="{{old('request_date')}}" style="font-size: 11px" name="request_date" type="date" required />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label  class="form-label" style="font-size: 12px">Select Vessel</label>
                                            <select id="vessel_id" class="form-control" style="font-size: 11px" name="vessel_id" required>
                                                <option value="" >Select Vessel</option>
                                                @foreach(\App\Vessel::all() as $v)
                                                    <option value="{{$v->id}}" {{ old('vessel_id')==$v->id ? 'selected' : ''  }} >{{$v->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <input type="hidden" id="idEdit" name="idEdit"/>
                                        <div class="col-md-12">
                                            <label  class="form-label" style="font-size: 12px">client Name</label>
                                            <select id="client_id" class="form-control" style="font-size: 11px" name="client_id">
                                                <option value="" >Select Tank</option>
                                                @foreach(\App\Client::all() as $s)
                                                    <option value="{{$s->id}}" {{ old('client_id')==$s->id ? 'selected' : ''  }} >{{$s->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>





                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label  class="form-label" style="font-size: 12px">Product Name</label>
                                            <select id="product_id" class="form-control" style="font-size: 11px" name="product_id">
                                                <option value="" >Select Tank</option>
                                                @foreach(\App\Product::all() as $s)
                                                    <option value="{{$s->id}}"  {{ old('product_id')==$s->id ? 'selected' : ''  }}>{{$s->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-12">
                                            <label  class="form-label" style="font-size: 12px">Quantity Refined</label>
                                            <input id="refined_quantity" name="refined_quantity"  class="form-control" type="number" min="0" value="0" step="0.001"style="font-size: 11px" required=""readonly >
                                        </div>

                                    </div>
                                    <div class ="row">
                                        <div class="col-md-12">
                                            <label  class="form-label" style="font-size: 12px">Remaining Quantity</label>
                                            <input id="remain_quantity" name="remain_quantity"  class="form-control" type="number" min="0" value="0" step="0.001" style="font-size: 11px" required="" readonly>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label  class="form-label" style="font-size: 12px">Sell Quantity</label>
                                            <input id="sell_quantity" name="sell_quantity"  class="form-control" type="number" min="0" value="0" step="0.001" style="font-size: 11px" required="" >


                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="submit" id="savee" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-save"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>



    <script src="assets/js/vendor.min.js"></script>
    <script src="assets/js/elephant.min.js"></script>
    <script src="assets/js/application.min.js"></script>
    <script src="assets/js/demo.min.js"></script>
    <script src="assets/parsley/js/parsley.min.js"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-83990101-1', 'auto');
        ga('send', 'pageview');
    </script>
    <script src="assets/js/toastr.min.js"></script>



    <script>
        $(document).on('click','.editbtn',function(){
            $('#product_idEdit').val($(this).data('product_id')).select();
            $('#idEdit').val($(this).data('id'));
            $('#client_idEdit').val($(this).data('client_id')).select();
            $('#vessel_idEdit').val($(this).data('vessel_id')).select();
//         $('#refined_quantityEdit').val($(this).data('refined_quantity'));
            $('#sell_quantityEdit').val($(this).data('sell_quantity'));
            $('#request_dateEdit').val($(this).data('request_date'));
//
             var product_id=$(this).data('product_id');


            $.ajax({
                type: 'POST',
                url: '{{URL::to('quantity_refined_for_product')}}',
                data: {
                    '_token': "{{ csrf_token() }}",
                    'product_id':product_id,
                },
                success: function (data) {
                    var x= data.split("|");
                    $('#refined_quantityEdit').val(parseFloat(x[0]).toFixed(3));
//
                    $('#remain_quantityEdit').val((parseFloat(x[0])- parseFloat(x[1])).toFixed(3));
                }
            })
//



        });
    </script>
    <script>
        $(document).on('click','.deletebtn',function() {
            $('#idDelete').val($(this).data('id'));
            $("#nameDelete").html($(this).data('client_id'));

        });
    </script>
    <script>
        $(document).on('change','#product_id',function(e) {

            var product_id=$('#product_id').val();


            $.ajax({
                type: 'POST',
                url: '{{URL::to('quantity_refined_for_product')}}',
                data: {
                    '_token': "{{ csrf_token() }}",
                    'product_id':product_id,
                },
                success: function (data) {
                   // alert(data);
                    var x= data.split("|");
                    $('#refined_quantity').val(parseFloat(x[0]).toFixed(3));
//
                    $('#remain_quantity').val((parseFloat(x[0])- parseFloat(x[1])).toFixed(3));
                }
            })

        });
        $(document).on('change','#product_idEdit',function(e) {

            var product_id=$('#product_idEdit').val();


            $.ajax({
                type: 'POST',
                url: '{{URL::to('quantity_refined_for_product')}}',
                data: {
                    '_token': "{{ csrf_token() }}",
                    'product_id':product_id,
                },
                success: function (data) {
                    var x= data.split("|");
                    $('#refined_quantityEdit').val(parseFloat(x[0]).toFixed(3));
//
                    $('#remain_quantityEdit').val((parseFloat(x[0])- parseFloat(x[1])).toFixed(3));
                }
            })

        });
    </script>

    <script>
        $("#updaterequest").submit(function(e){
            e.preventDefault();
            var formdata = $(this).serialize(); // here $(this) refere to the form its submitting

            $.ajax({
                type: 'POST',
                url: "{{ url('/updaterequest') }}",
                data: formdata, // here $(this) refers to the ajax object not form
                success: function (data) {
                    alert("Record Updated Successfully");
                },
            });

        });
    </script>
</body>
</html>