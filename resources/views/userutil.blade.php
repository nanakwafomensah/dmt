<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>  DMT Crude Oil processing</title>
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta property="og:url" content="">
    <meta property="og:type" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:creator" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
    <link rel="apple-touch-icon" sizes="180x180" href="">
    <link rel="icon" type="image/png" href="favicon.ico" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon.ico" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#27ae60">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="assets/css/vendor.min.css">
    <link rel="stylesheet" href="assets/css/elephant.min.css">
    <link rel="stylesheet" href="assets/css/application.min.css">
    <link rel="stylesheet" href="assets/css/demo.min.css">
    <link rel="stylesheet" href="assets/parsley/css/parsley.css">
</head>
<body class="layout layout-header-fixed">
<div class="layout-header">
    @include('template.topbar')
</div>
<div class="layout-main">
    <div class="layout-sidebar" >
        <div class="layout-sidebar-backdrop" style="background-color: #217345;"></div>
        <div class="layout-sidebar-body" style="background-color: #217345;">
            <div class="custom-scrollbar" >
                <nav id="sidenav" class="sidenav-collapse collapse" style="background-color: #217345; color: #fff">
                    <ul class="sidenav">




                        <li class="sidenav-heading">Production Setting</li>
                        <li class="sidenav-item ">
                            <a href="vessel" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-truck"></span>
                                <span class="sidenav-label" style="font-size: 11px">Vessel</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="tank" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-beer"></span>
                                <span class="sidenav-label" style="font-size: 11px">Tank</span>
                            </a>

                        </li>

                        <li class="sidenav-item">
                            <a href="product" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-group"></span>
                                <span class="sidenav-label" style="font-size: 11px">Refined Product</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="client" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-user"></span>
                                <span class="sidenav-label" style="font-size: 11px">Off Takers</span>
                            </a>

                        </li>




                        <li class="sidenav-heading">Depot Record</li>
                        <li class="sidenav-item ">
                            <a href="consignment" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">New Crude Oil </span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="refined" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">New Product</span>
                            </a>

                        </li>

                        <li class="sidenav-heading">Sale</li>
                        <li class="sidenav-item">
                            <a href="request" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-archive"></span>
                                <span class="sidenav-label" style="font-size: 11px">New Release</span>
                            </a>
                        </li>
                        <li class="sidenav-item">
                            <a href="load" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-suitcase"></span>
                                <span class="sidenav-label" style="font-size: 11px">Loading</span>
                            </a>
                        </li>
                        <li class="sidenav-heading">Report</li>
                        <li class="sidenav-item ">
                            <a href="report" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Summary Report</span>
                            </a>
                        </li>
                        <li class="sidenav-item ">
                            <a href="reportother" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Other Report</span>
                            </a>
                        </li>
                        <li class="sidenav-item">
                            <a href="remark" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Remarks</span>
                            </a>
                        </li>
                        <li class="sidenav-heading">User Management</li>

                        <li class="sidenav-item ">
                            <a href="#createUser" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-user"></span>
                                <span class="sidenav-label" style="font-size: 11px" data-toggle="modal" data-target="#createUser">Create New User</span>
                            </a>

                        </li>
                        <li class="sidenav-item active ">
                            <a href="userutil" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-edit"></span>
                                <span class="sidenav-label" style="font-size: 11px" >User Utilities</span>
                            </a>

                        </li>

                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">

                <h1 class="title-bar-title">
                    <span class="d-ib">User Management</span>
                </h1>
                <p class="title-bar-description">
                    <small  style ="color:#217345">Utilities</small>

                </p>
            </div>
            <div class="row gutter-xs">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-actions">

                                <button type="button" class="card-action card-reload" title="Reload"></button>

                            </div>

                        </div>
                        <div class="card-body">
                            <table id="demo-datatables-responsive-1" class="table table-bordered table-striped table-nowrap dataTable" cellspacing="0" width="100%" style="font-size: 12px">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>email</th>
                                    <th>username</th>
                                    <th>Sex</th>
                                    <th>Location</th>

                                    <th><Center> Actions</Center></th>


                                </tr>
                                </thead>
                                <tbody>
                               @foreach(Cartalyst\Sentinel\Users\EloquentUser::all() as $u)
                                <tr>
                                    <td>{{$u->first_name ." ".$u->last_name}}</td>
                                    <td>{{$u->email}}</td>
                                    <td>{{$u->username}}</td>
                                    <td>{{$u->sex}}</td>
                                    <td>{{$u->location}}</td>


                                    <td><Center>
                                            <button class="edit-modal btn  btn-warning" style = "text-transform: capitalize; background-color: goldenrod; ; border-color: goldenrod"  type="button" data-toggle="modal" data-target="#editUser" >  <i class = "icon icon-edit"></i></button> |
                                             <button class="deletebtn btn  btn-warning" style = "text-transform: capitalize;  background-color: maroon; border-color: maroon"  type="button" data-toggle="modal" data-target="#delUser" > <i class = "icon icon-trash"></i></button>
                                        </Center></td>




                                </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            {{--<div class="divider">--}}
            {{--<div class="divider-content " style = "color: #217345"><b><i class="icon icon-line-chart"></i> Statistics</b></div>--}}
            {{--</div>--}}







        </div>

        @include('template.logoutView')

        @include('template.createuserView')

        @include('template.changepasswordview')

        <div id="delUser" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <form action="UserRoles/deleteuser" method="post">
                    <div class="modal-content">
                        <div class="modal-header bg-primary" style="background-color: #217345">
                            <button type="button" class="close" data-dismiss="modal" style="color: #fff">
                                <span aria-hidden="true" style="color: #fff">×</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <div class="text-center">
                                <span class="icon icon-trash icon-5x m-y-lg"></span>
                                <h4 class="modal-title" style="font-size: 14px">User Account Delete</h4>

                            </div>
                        </div>
                        <input type="hidden" name="user_de" id="user_de"/>
                        <div class="modal-tabs">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="active"><a href="#display" data-toggle="tab" style="font-size: 12px">Do You Want To Delete <span id="usersfullname_de"></span> From LoyalStar?</a></li>

                            </ul>

                        </div>
                        <div class="modal-footer">
                            <center><button type="submit" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-check"></i>&nbsp &nbsp Yes &nbsp &nbsp</button> <button type="button" class="btn btn-danger" style="background-color: maroon"><i class="icon icon-close"></i> Cancel</button></center>

                        </div>
                    </div>
                </form>
            </div>
        </div>


        <div id="disUser" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <form action="UserRoles/disableuser" method="post">
                    <div class="modal-content">
                        <div class="modal-header bg-primary" style="background-color: #217345">
                            <button type="button" class="close" data-dismiss="modal" style="color: #fff">
                                <span aria-hidden="true" style="color: #fff">×</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <div class="text-center">
                                <span class="icon icon-lock icon-5x m-y-lg"></span>
                                <h4 class="modal-title" style="font-size: 14px">Disable User Account</h4>

                            </div>
                        </div>
                        <input type="hidden" name="user_di" id="user_di"/>
                        <div class="modal-tabs">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="active"><a href="#display" data-toggle="tab" style="font-size: 12px">Do You Want To Disable  <span id="usersfullname_di"></span>  From LoyalStar?</a></li>

                            </ul>

                        </div>
                        <div class="modal-footer">
                            <center><button type="submit" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-check"></i>&nbsp &nbsp Yes &nbsp &nbsp</button> <button type="button" class="btn btn-danger" style="background-color: maroon"><i class="icon icon-close"></i> Cancel</button></center>

                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div id="disUserk" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <form action="UserRoles/lockuser" method="post">
                    <div class="modal-content">
                        <div class="modal-header bg-primary" style="background-color: #217345">
                            <button type="button" class="close" data-dismiss="modal" style="color: #fff">
                                <span aria-hidden="true" style="color: #fff">×</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <div class="text-center">
                                <span class="icon icon-lock icon-5x m-y-lg"></span>
                                <h4 class="modal-title" style="font-size: 14px">Lock User Account</h4>

                            </div>
                        </div>
                        <input type="hidden" name="user_loc" id="user_loc"/>
                        <div class="modal-tabs">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="active"><a href="#display" data-toggle="tab" style="font-size: 12px">Do You Want To lock <span id="usersfullname_loc"></span>  From LoyalStar?</a></li>

                            </ul>

                        </div>
                        <div class="modal-footer">
                            <center><button type="submit" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-check"></i>&nbsp &nbsp Yes &nbsp &nbsp</button> <button type="button" class="btn btn-danger" style="background-color: maroon"><i class="icon icon-close"></i> Cancel</button></center>

                        </div>
                    </div>
                </form>
            </div>
        </div>


        <div id="editUser" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <form action="UserRoles/updateuser" method="post" data-parsley-validate="" id="editform">
                    <div class="modal-content">
                        <div class="modal-header bg-primary" style="background-color: #217345">
                            <button type="button" class="close" data-dismiss="modal" style="color: #fff" >
                                <span aria-hidden="true" style="color: #fff">×</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <div class="text-center">
                                <span class="icon icon-edit icon-5x m-y-lg"></span>
                                <h4 class="modal-title" style="font-size: 12px">Edit User's Details</h4>

                            </div>
                        </div>
                        <div class="modal-tabs">

                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="display4">

                                    <div class="form-group">
                                        <div class="row">
                                            <input type="hidden" name="userid_e" id="userid_e"/>
                                            <div class="col-md-12">
                                                <label  class="form-label" style="font-size: 12px">User's Full Name</label>
                                                <input id="usersfullname_e" name="usersfullname_e" class="form-control" type="text" style="font-size: 11px" required></div>



                                        </div></div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label  class="form-label" style="font-size: 12px">Sex</label>

                                                <select id="sex_e" name="sex_e" class="form-control" style="font-size: 11px" required>
                                                    <option value="M" >Male</option>
                                                    <option value="F">Sex</option>
                                                    <option value="O">Other</option>

                                                </select>
                                            </div>


                                            <div class="col-md-6">
                                                <label  class="form-label" style="font-size: 12px">Phone Number</label>
                                                <input id="phonenumber_e" name="phonenumber_e" class="form-control" type="text" style="font-size: 11px" maxlength="10" pattern="[0-9]{10}" required>
                                            </div>

                                        </div>
                                    </div><hr style="border-color: #217345">

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label  class="form-label" style="font-size: 12px">User Role</label>
                                                <select id="rolename_e" class="form-control" style="font-size: 11px" name="rolename_e" required>

                                                    <option value="1">helllo</option>



                                                </select>
                                            </div>



                                        </div>
                                    </div>




                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">

                            <button type="submit" id="save_d" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-save"></i> Save</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>




        <div id="createUserP" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <form action="UserRoles/storeuserparent" method="post" data-parsley-validate="">
                    <div class="modal-content">
                        <div class="modal-header bg-primary" style="background-color: #217345">
                            <button type="button" class="close" data-dismiss="modal" style="color: #fff" >
                                <span aria-hidden="true" style="color: #fff">×</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <div class="text-center">
                                <span class="icon icon-user-plus icon-5x m-y-lg"></span>
                                <h4 class="modal-title" style="font-size: 12px">Create A New Parent User</h4>

                            </div>
                        </div>
                        <div class="modal-tabs">

                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="display2">

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <label  class="form-label" style="font-size: 12px">Company name</label>
                                                <select id="demo-select2-2" class="form-control" style="font-size: 11px" name="companyname" required>
                                                    <option value="">Choose an OMC</option>
                                                    <option value="frimpsoil">FRIMPS OIL</option>
                                                    <option value="goil">GOIL</option>


                                                </select>
                                                <div class="col-md-4">
                                                    <!--                                            <label  class="form-label" style="font-size: 12px">Sex</label>-->
                                                    <!---->
                                                    <!--                                            <select id="demo-select2-2" class="form-control" style="font-size: 11px" name="sex">-->
                                                    <!--                                                <option value="M" >Male</option>-->
                                                    <!--                                                <option value="F">Female</option>-->
                                                    <!--                                                <option value="O">Other</option>-->
                                                    <!---->
                                                    <!--                                            </select>-->
                                                </div>

                                            </div></div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <label  class="form-label" style="font-size: 12px">User Email</label>
                                                    <input id="form-control-7" class="form-control" type="email" name="email" style="font-size: 11px" required="">
                                                </div>


                                                <div class="col-md-4">
                                                    <label  class="form-label" style="font-size: 12px">Phone Number</label>
                                                    <input id="form-control-10" class="form-control" type="text" name="phonenumber" style="font-size: 11px" required="" maxlength="10" pattern="[0-9]{10}">
                                                </div>

                                            </div>
                                        </div><hr style="border-color: #217345">

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label  class="form-label" style="font-size: 12px">User Role</label>
                                                    <select id="demo-select2-3" class="form-control" name="userrole" style="font-size: 11px" disabled>

                                                        <option value="1" >admin </option>


                                                    </select>
                                                </div>



                                            </div>
                                        </div>




                                    </div>

                                </div>
                            </div>
                            <div class="modal-footer">

                                <button type="submit" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-save"></i> Save</button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="assets/js/vendor.min.js"></script>
<script src="assets/js/elephant.min.js"></script>
<script src="assets/js/application.min.js"></script>
<script src="assets/js/demo.min.js"></script>
<script src="assets/parsley/js/parsley.min.js"></script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-83990101-1', 'auto');
    ga('send', 'pageview');
</script>

</body>
</html>