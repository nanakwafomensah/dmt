<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>  DMT Crude Oil processing</title>
<meta name="viewport" content="">
<meta name="description" content="">
<meta property="og:url" content="">
<meta property="og:type" content="">
<meta property="og:title" content="">
<meta property="og:description" content="">
<meta property="og:image" content="">
<meta name="twitter:card" content="">
<meta name="twitter:site" content="">
<meta name="twitter:creator" content="">
<meta name="twitter:title" content="">
<meta name="twitter:description" content="">
<meta name="twitter:image" content="">
<link rel="apple-touch-icon" sizes="180x180" href="">
<link rel="icon" type="image/png" href="favicon.ico" sizes="32x32">
<link rel="icon" type="image/png" href="favicon.ico" sizes="16x16">
<link rel="manifest" href="manifest.json">
<link rel="mask-icon" href="safari-pinned-tab.svg" color="#27ae60">
<meta name="theme-color" content="#ffffff">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
<link rel="stylesheet" href="assets/css/vendor.min.css">
<link rel="stylesheet" href="assets/css/elephant.min.css">
<link rel="stylesheet" href="assets/css/application.min.css">
<link rel="stylesheet" href="assets/css/demo.min.css">
<link rel="stylesheet" href="assets/parsley/css/parsley.css">

</head>
<body class="layout layout-header-fixed">
<div class="layout-header">
@include('template.topbar')
</div>
<div class="layout-main">
    <div class="layout-sidebar" >
        <div class="layout-sidebar-backdrop" style="background-color: #217345;"></div>
        <div class="layout-sidebar-body" style="background-color: #217345;">
            <div class="custom-scrollbar" >
                <nav id="sidenav" class="sidenav-collapse collapse" style="background-color: #217345; color: #fff">
                    <ul class="sidenav">
                        <li class="sidenav-heading">Production Setting</li>
                        <li class="sidenav-item ">
                            <a href="vessel" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-truck"></span>
                                <span class="sidenav-label" style="font-size: 11px">Vessel</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="tank" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-beer"></span>
                                <span class="sidenav-label" style="font-size: 11px">Tank</span>
                            </a>

                        </li>

                        <li class="sidenav-item ">
                            <a href="product" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-group"></span>
                                <span class="sidenav-label" style="font-size: 11px">Refined Product</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="client" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-user"></span>
                                <span class="sidenav-label" style="font-size: 11px">Off Takers</span>
                            </a>

                        </li>




                        <li class="sidenav-heading">Depot Record</li>
                        <li class="sidenav-item ">
                            <a href="consignment" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">New Crude Oil </span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="refined" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">New Product</span>
                            </a>

                        </li>

                        <li class="sidenav-heading">Sale</li>
                        <li class="sidenav-item">
                            <a href="request" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-archive"></span>
                                <span class="sidenav-label" style="font-size: 11px">New Release</span>
                            </a>
                        </li>
                        <li class="sidenav-item active">
                            <a href="load" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-suitcase"></span>
                                <span class="sidenav-label" style="font-size: 11px">Loading</span>
                            </a>
                        </li>
                        <li class="sidenav-heading">Report</li>
                        <li class="sidenav-item ">
                            <a href="report" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Summary Report</span>
                            </a>
                        </li>
                        <li class="sidenav-item ">
                            <a href="reportother" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Other Report</span>
                            </a>
                        </li>
                        <li class="sidenav-item">
                            <a href="remark" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Remarks</span>
                            </a>
                        </li>
                        <li class="sidenav-heading">User Management</li>

                        <li class="sidenav-item ">
                            <a href="#createUser" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-user"></span>
                                <span class="sidenav-label" style="font-size: 11px" data-toggle="modal" data-target="#createUser">Create New User</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="userutil" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-edit"></span>
                                <span class="sidenav-label" style="font-size: 11px" >User Utilities</span>
                            </a>

                        </li>

                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">

                <h1 class="title-bar-title">
                    <span class="d-ib">lifting </span>

                </h1>
                <p class="title-bar-description">
                    <small  style ="color:#217345">Utilities</small>

                </p>
            </div>


            <div class="pull-left">
                <button class=" btn  btn-success" style = "text-transform: capitalize; background-color: #217345" data-toggle="modal" data-target="#newDistrict" type="button"> <i class = "icon icon-plus-circle"></i> &nbsp New Lifting</button>


            </div>
            <br><br>
            <div class="row">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-actions">

                                <button type="button" class="card-action card-reload" title="Reload"></button>

                            </div>

                        </div>
                        <div class="card-body">

                            <table id="demo-datatables-responsive-1" class="table table-bordered table-striped table-nowrap dataTable" cellspacing="0" width="100%" style="font-size: 12px">
                                <thead>
                                <tr>
                                    <th> Number</th>
                                    <th> Date</th>

                                    <th> Client</th>
                                    <th> Product</th>
                                    <th> Lifted Quantity(Litres)</th>
                                    <th> Vessel</th>


                                    <th><center>Action</center></th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php $x=1; ?>
                                @foreach($liftings as $p)

                                    <tr>
                                        <td>
                                            {{$x++}}
                                        </td>
                                        <td>
                                            {{date("D M jS, Y", strtotime($p->lift_date))}}
                                        </td>
                                        <td>
                                            {{\App\Client::find($p->client_id)->name}}
                                        </td>
                                        <td>
                                            {{\App\Product::find($p->product_id)->name}}
                                        </td>
                                        <td>
                                            {{$p->lifted_quantity }}
                                        </td>
                                        <td>
                                            {{\App\Vessel::find($p->vessel_id)->name}}
                                        </td>


                                        <td>
                                            <center>
                                                <button class="deletebtn btn  btn-warning" style = "text-transform: capitalize;  background-color: maroon; ; border-color: maroon"  type="button" data-toggle="modal" data-target="#delDistrict"
                                                        data-id="{{$p->id}}"
                                                        data-client_id="{{\App\Client::find($p->client_id)->name}}"

                                                > <i class = "icon icon-trash"></i></button>
                                                <button class="editbtn btn  btn-warning" style = "text-transform: capitalize;  background-color: goldenrod; ; border-color: goldenrod"  type="button" data-toggle="modal" data-target="#editDistrict" data-id="{{$p->id}}" data-name="{{$p->name}}"

                                                        data-client_id="{{$p->client_id}}"
                                                        data-product_id="{{$p->product_id}}"
                                                        data-vessel_id="{{$p->vessel_id}}"
                                                        data-daily_sale="{{$p->daily_sale}}"
                                                        data-lifted_quantity="{{$p->lifted_quantity}}"
                                                        data-remain_lifted_quantity="{{$p->remain_lifted_quantity}}"
                                                        data-lift_date="{{$p->lift_date}}"
                                                > <i class = "icon icon-edit"></i></button>
                                            </center>
                                        </td>

                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    @include('template.logoutView')

    @include('template.createuserView')

    @include('template.changepasswordview')

    <div id="delDistrict" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <form action="deleteload" method="POST">
                {{csrf_field()}}
                <div class="modal-content">
                    <div class="modal-header bg-primary" style="background-color: #217345">
                        <button type="button" class="close" data-dismiss="modal" style="color: #fff">
                            <span aria-hidden="true" style="color: #fff">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <div class="text-center">
                            <span class="icon icon-trash icon-5x m-y-lg"></span>
                            <h4 class="modal-title" style="font-size: 14px">Lifting</h4>

                        </div>
                    </div>
                    <div class="modal-tabs">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#display" data-toggle="tab" style="font-size: 12px">Do You Want To Delete <span id="nameDelete"></span> Load From System?</a></li>

                        </ul>
                        <input type="hidden" id="idDelete" name="idDelete"/>

                    </div>
                    <div class="modal-footer">
                        <center><button type="submit" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-check"></i>&nbsp &nbsp Yes &nbsp &nbsp</button>
                            <button type="button" class="btn btn-danger" style="background-color: maroon"><i class="icon icon-close"></i> Cancel</button></center>

                    </div>
                </div>
            </form>
        </div>
    </div>


    <div id="editDistrict" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <form action="updateload" method="post" data-parsley-validate="" id="updateload">
                {{csrf_field()}}
                <div class="modal-content">
                    <div class="modal-header bg-primary" style="background-color: #217345">
                        <button type="button" class="close" data-dismiss="modal" style="color: #fff" >
                            <span aria-hidden="true" style="color: #fff">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <div class="text-center">
                            <span class="icon icon-edit icon-5x m-y-lg"></span>
                            <h4 class="modal-title" style="font-size: 12px">Edit Lifting Details</h4>

                        </div>
                    </div>
                    <div class="modal-tabs">

                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="display3">


                                <div class="form-group">
                                    <div class="row">
                                           <input name="idEdit" id="idEdit" type="hidden"/>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label  class="form-label" style="font-size: 12px">Date</label>
                                                <input id="lift_dateEdit" class="form-control" style="font-size: 11px" name="lift_dateEdit" type="date" required />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label  class="form-label" style="font-size: 12px">vessel</label>
                                            <select id="vessel_idEdit" class="form-control" style="font-size: 11px" name="vessel_idEdit">
                                                <option value="" >Select Vessel</option>
                                                @foreach(\App\Vessel::all() as $s)
                                                    <option value="{{$s->id}}" >{{$s->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label  class="form-label" style="font-size: 12px">client Name</label>
                                            <select id="client_idEdit" class="form-control" style="font-size: 11px" name="client_idEdit">
                                                <option value="" >Select Tank</option>
                                                @foreach(\App\Client::all() as $s)
                                                    <option value="{{$s->id}}" >{{$s->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label  class="form-label" style="font-size: 12px">Product Name</label>
                                            <select id="product_idEdit" class="form-control" style="font-size: 11px" name="product_idEdit">
                                                <option value="" >Select Tank</option>
                                                @foreach(\App\Product::all() as $s)
                                                    <option value="{{$s->id}}" >{{$s->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>


                                    </div><br>
                                    <center>
                                        <a class="btn btn-primary" style="background-color: #217345" id="view_requestedEdit">click to view Requested</a>

                                    </center>
                                    <center><h2><span id="product_requestEdit" style="color: #217345;font-weight: bold;"></span></h2></center>
                                    <hr>
                                    <div class="row">

                                        <div class="col-md-6">

                                            <label  class="form-label" style="font-size: 12px">Lifted Quantity</label>
                                            <input id="lifted_quantityEdit" name="lifted_quantityEdit"  class="form-control" type="number" min="0" value="0" step="0.001" style="font-size: 11px" required="" >

                                        </div>




                                    </div>



                                </div>



                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="submit" id="savee" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-save"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="newDistrict" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <form action="saveload" method="post" data-parsley-validate="" id="editform">
                {{csrf_field()}}
                <div class="modal-content">
                    <div class="modal-header bg-primary" style="background-color: #217345">
                        <button type="button" class="close" data-dismiss="modal" style="color: #fff" >
                            <span aria-hidden="true" style="color: #fff">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <div class="text-center">
                            <span class="icon icon-edit icon-5x m-y-lg"></span>
                            <h4 class="modal-title" style="font-size: 12px">Add Lifting </h4>

                        </div>
                    </div>
                    <div class="modal-tabs">

                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="display3">


                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label  class="form-label" style="font-size: 12px">Date</label>
                                            <input id="lift_date" value="{{old('lift_date')}}" class="form-control" style="font-size: 11px" name="lift_date" type="date" required />
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-4">
                                            <label  class="form-label" style="font-size: 12px">vessel</label>
                                            <select id="vessel_id" class="form-control" style="font-size: 11px" name="vessel_id">
                                                <option value="" >Select Vessel</option>
                                                @foreach(\App\Vessel::all() as $s)
                                                    <option value="{{$s->id}}" {{ old('vessel_id')==$s->id ? 'selected' : ''  }} >{{$s->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label  class="form-label" style="font-size: 12px">client Name</label>
                                            <select id="client_id" class="form-control" style="font-size: 11px" name="client_id">
                                                <option value="" >Select Tank</option>
                                                @foreach(\App\Client::all() as $s)
                                                    <option value="{{$s->id}}" {{ old('client_id')==$s->id ? 'selected' : ''  }} >{{$s->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label  class="form-label" style="font-size: 12px">Product Name</label>
                                            <select id="product_id" class="form-control" style="font-size: 11px" name="product_id">
                                                <option value="" >Select Tank</option>
                                                @foreach(\App\Product::all() as $s)
                                                    <option value="{{$s->id}}" {{ old('product_id')==$s->id ? 'selected' : ''  }} >{{$s->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>


                                    </div><br>
                                   <center>
                                       <a class="btn btn-primary" style="background-color: #217345" id="view_requested">click to view Requested</a>

                                   </center>
                                   <center><h2><span id="product_request" style="color: #217345;font-weight: bold;"></span></h2></center>
                                    <hr>
                                    <div class="row">

                                        <div class="col-md-6">

                                            <label  class="form-label" style="font-size: 12px">Lifted Quantity</label>
                                            <input id="lifted_quantity" name="lifted_quantity"  class="form-control" type="number" min="0" value="0" step="0.001" style="font-size: 11px" required="" >

                                        </div>




                                    </div>



                                </div>




                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="submit" id="savee" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-save"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>



    <script src="assets/js/vendor.min.js"></script>
    <script src="assets/js/elephant.min.js"></script>
    <script src="assets/js/application.min.js"></script>
    <script src="assets/js/demo.min.js"></script>
    <script src="assets/parsley/js/parsley.min.js"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-83990101-1', 'auto');
        ga('send', 'pageview');
    </script>
    <script src="assets/js/toastr.min.js"></script>



    <script>
        $(document).on('click','.editbtn',function(){
            $('#request_idEdit').val($(this).data('request_id'));
            $('#idEdit').val($(this).data('id'));
            $('#client_idEdit').val($(this).data('client_id')).select();
            $('#vessel_idEdit').val($(this).data('vessel_id')).select();
            $('#product_idEdit').val($(this).data('product_id')).select();
            $('#lifted_quantityEdit').val($(this).data('lifted_quantity'));
            $('#remain_lifted_quantityEdit').val($(this).data('remain_lifted_quantity'));
            $('#lift_dateEdit').val($(this).data('lift_date'));



        });
        $(document).on('click','#view_requested',function(){
         var vessel_id= $('#vessel_id').val();
            var client_id= $('#client_id').val();
            var product_id =$('#product_id').val();

            $.ajax({
                type: 'POST',
                url: '{{URL::to('product_request')}}',
                data: {
                    '_token': "{{ csrf_token() }}",
                    'product_id':product_id,
                    'client_id':client_id,
                    'vessel_id':vessel_id,
                },
                success: function (data) {
                    $('#product_request').html(parseFloat(data).toFixed(3));


                }
            })
        });
        $(document).on('click','#view_requestedEdit',function(){
            var vessel_id= $('#vessel_idEdit').val();
            var client_id= $('#client_idEdit').val();
            var product_id =$('#product_idEdit').val();

            $.ajax({
                type: 'POST',
                url: '{{URL::to('product_request')}}',
                data: {
                    '_token': "{{ csrf_token() }}",
                    'product_id':product_id,
                    'client_id':client_id,
                    'vessel_id':vessel_id,

                },
                success: function (data) {
                    $('#product_requestEdit').html(parseFloat(data).toFixed(3));
                }
            })
        });
    </script>
    <script>
        $(document).on('click','.deletebtn',function() {
            $('#idDelete').val($(this).data('id'));
            $("#nameDelete").html($(this).data('client_id'));

        });
    </script>
    <script>
        $("#updateload").submit(function(e){
            e.preventDefault();
            var formdata = $(this).serialize(); // here $(this) refere to the form its submitting

            $.ajax({
                type: 'POST',
                url: "{{ url('/updateload') }}",
                data: formdata, // here $(this) refers to the ajax object not form
                success: function (data) {
                    alert("Record Updated Successfully");
                },
            });

        });
    </script>

    </body>
    </html>