
<div id="createUser" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <form action="saveuser" method="post" data-parsley-validate="">
            {{csrf_field()}}
            <div class="modal-content">
                <div class="modal-header bg-primary" style="background-color: #217345">
                    <button type="button" class="close" data-dismiss="modal" style="color: #fff" >
                        <span aria-hidden="true" style="color: #fff">×</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <div class="text-center">
                        <span class="icon icon-user-plus icon-5x m-y-lg"></span>
                        <h4 class="modal-title" style="font-size: 12px">Create A New User</h4>

                    </div>
                </div>
                <div class="modal-tabs">

                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="display2">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-8">
                                        <label  class="form-label" style="font-size: 12px">User's Full Name</label>
                                        <input id="form-control-6" class="form-control" type="text" style="font-size: 11px" name="fullname" required=""></div>

                                    <div class="col-md-4">
                                        <label  class="form-label" style="font-size: 12px">Sex</label>

                                        <select id="demo-select2-2" class="form-control" style="font-size: 11px" name="sex">
                                            <option value="" >Select Gender</option>
                                            <option value="male" >Male</option>
                                            <option value="female">Female</option>


                                        </select>
                                    </div>

                                </div></div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-8">
                                        <label  class="form-label" style="font-size: 12px">User Email</label>
                                        <input id="form-control-7" class="form-control" type="email" name="email" style="font-size: 11px" required="">
                                    </div>
                                    <div class="col-md-4">
                                        <label  class="form-label" style="font-size: 12px">Phone Number</label>
                                        <input id="form-control-10" class="form-control" type="text" name="phonenumber" style="font-size: 11px" required="" maxlength="10" pattern="[0-9]{10}">
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label  class="form-label" style="font-size: 12px">Password</label>
                                        <input id="form-control-7" class="form-control"  name="password" style="font-size: 11px" required="">
                                    </div>
                                    <div class="col-md-4">
                                        <label  class="form-label" style="font-size: 12px">Username</label>
                                        <input id="form-control-7" class="form-control"  name="username" style="font-size: 11px" required="">
                                    </div>
                                    <div class="col-md-4">
                                        <label  class="form-label" style="font-size: 12px">location</label>
                                        <input id="form-control-7" class="form-control"  name="location" style="font-size: 11px" required="">
                                    </div>
                                </div>
                            </div>
                            <hr style="border-color: #217345">






                        </div>

                    </div>
                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-save"></i> Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
