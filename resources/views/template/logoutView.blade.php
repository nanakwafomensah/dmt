<div id="logout" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary" style="background-color: #217345">
                <button type="button" class="close" data-dismiss="modal" style="color: #fff">
                    <span aria-hidden="true" style="color: #fff">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <div class="text-center">
                    <span class="icon icon-lock icon-5x m-y-lg"></span>
                    <h4 class="modal-title" style="font-size: 14px">System Logout</h4>

                </div>
            </div>
            <div class="modal-tabs">
                <ul class="nav nav-tabs nav-justified">
                    <li class="active"><a href="#display" data-toggle="tab" style="font-size: 12px">Do You Want To Logout From the System?</a></li>

                </ul>

            </div>
            <div class="modal-footer">
                <form action="{{route('logout')}}" method="post" id="logout-form">
                    {{csrf_field()}}
                    <center> <a href="#" onclick="document.getElementById('logout-form').submit()" type="button" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-check"></i> Logout</a><button type="button" class="btn btn-danger" style="background-color: maroon" data-dismiss="modal"><i class="icon icon-close"></i> Cancel</button></center>

                </form>

            </div>
        </div>
    </div>
</div>