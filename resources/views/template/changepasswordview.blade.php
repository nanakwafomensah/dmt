<div id="pwdChange" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header bg-primary" style="background-color: #217345">
                <button type="button" class="close" data-dismiss="modal" style="color: #fff" >
                    <span aria-hidden="true" style="color: #fff">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <div class="text-center">
                    <span class="icon icon-edit icon-5x m-y-lg"></span>
                    <h4 class="modal-title" style="font-size: 12px">Change Password</h4>

                </div>
            </div>
            <div class="modal-tabs">

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="display">
                        <input type="hidden" name="user_id_s" id="user_id_s" value=""/>
                        <div class="form-group">

                            <label  class="form-label" style="font-size: 12px">Old Password</label>
                            <input  class="form-control" id="old_password" name="old_password" type="password" style="font-size: 11px">

                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label  class="form-label" style="font-size: 12px">New Password</label>
                                    <input  class="form-control" id="new_password" name="new_password" type="password" style="font-size: 11px">

                                </div>


                                <div class="col-md-6">
                                    <label  class="form-label" style="font-size: 12px">Confirm Password</label>
                                    <input  class="form-control" id="new_confirm_password" name="new_confirm_password" type="password" style="font-size: 11px"></div>

                            </div></div>


                    </div>

                </div>
            </div>
            <div class="modal-footer">

                <button id="changepassword" type="button" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-save"></i> Save</button>
            </div>
        </div>


    </div>
</div>