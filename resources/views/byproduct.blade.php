<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DMT crude Oil Processing</title>
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta property="og:url" content="">
    <meta property="og:type" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:creator" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
    <link rel="apple-touch-icon" sizes="180x180" href="">
    <link rel="icon" type="image/png" href="favicon.ico" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon.ico" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#27ae60">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="assets/css/vendor.min.css">
    <link rel="stylesheet" href="assets/css/elephant.min.css">
    <link rel="stylesheet" href="assets/css/application.min.css">
    <link rel="stylesheet" href="assets/css/demo.min.css">
    <link rel="stylesheet" href="assets/parsley/css/parsley.css">
    <script type='text/javascript' src='js/globalization/en-US.js'></script>
    <script type='text/javascript' src='js/core.js'></script>
    <script type='text/javascript' src='js/sugarpak.js'></script>
    <script type='text/javascript' src='js/parser.js'></script>
    <script type='text/javascript' src='js/extras.js'></script>
    <script type='text/javascript' src='js/time.js'></script>
</head>
<body class="layout layout-header-fixed">
<div class="layout-header">
    @include('template.topbar')
</div>
<div class="layout-main">
    <div class="layout-sidebar" >
        <div class="layout-sidebar-backdrop" style="background-color: #217345;"></div>
        <div class="layout-sidebar-body" style="background-color: #217345;">
            <div class="custom-scrollbar" >
                <nav id="sidenav" class="sidenav-collapse collapse" style="background-color: #217345; color: #fff">
                    <ul class="sidenav">
                        <li class="sidenav-heading">Production Settings</li>

                        <li class="sidenav-item ">
                            <a href="vessel" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-truck"></span>
                                <span class="sidenav-label" style="font-size: 11px">Vessel</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="tank" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-beer"></span>
                                <span class="sidenav-label" style="font-size: 11px">Tank</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="product" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-group"></span>
                                <span class="sidenav-label" style="font-size: 11px">Refined Product</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="client" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-user"></span>
                                <span class="sidenav-label" style="font-size: 11px">Off Takers</span>
                            </a>

                        </li>




                        <li class="sidenav-heading">Depot Record</li>
                        <li class="sidenav-item ">
                            <a href="consignment" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">New</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="refined" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">New Rroduct</span>
                            </a>

                        </li>

                        <li class="sidenav-heading">Sale</li>
                        <li class="sidenav-item">
                            <a href="request" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-archive"></span>
                                <span class="sidenav-label" style="font-size: 11px">New Release</span>
                            </a>
                        </li>
                        <li class="sidenav-item">
                            <a href="load" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-suitcase"></span>
                                <span class="sidenav-label" style="font-size: 11px">Loading</span>
                            </a>
                        </li>
                        <li class="sidenav-heading">Report</li>
                        <li class="sidenav-item">
                            <a href="report" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Summary Report</span>
                            </a>
                        </li>
                        <li class="sidenav-item ">
                            <a href="reportother" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Other Report</span>
                            </a>
                        </li>
                        <li class="sidenav-item">
                            <a href="remark" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Remarks</span>
                            </a>
                        </li>

                        <li class="sidenav-heading">User Management</li>

                        <li class="sidenav-item ">
                            <a href="#createUser" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-user"></span>
                                <span class="sidenav-label" style="font-size: 11px" data-toggle="modal" data-target="#createUser">Create New User</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="userutil" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-edit"></span>
                                <span class="sidenav-label" style="font-size: 11px" >User Utilities</span>
                            </a>

                        </li>

                    </ul>
                </nav>

            </div>
        </div>
    </div>
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">

                <h1 class="title-bar-title">
                    <span class="d-ib">Refined Product</span>
                </h1>
                <p class="title-bar-description">
                    <small  style ="color:#217345">Insights &amp Analytics</small>

                </p>
            </div><br><br>
            <div class = "row">

                    <div class="col-lg-3">

                        <div class="card">

                            <div class="card-header no-border">
                                <div class="col-md-12">
                                    <div class="demo-form-wrapper">

                                        <div class="form-group">
                                            <label for="demo-select2-6" class="form-label" style="font-size: 11px"><u>Refined product</u></label>
                                            <select id="byproduct" class="form-control" style="font-size: 11px" name="byproduct">
                                                <option value="" >Select product</option>
                                                @foreach(\App\Product::all() as $l)
                                                <option value="{{$l->id}}">{{$l->name}}</option>
                                                @endforeach

                                            </select>
                                        </div>


                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                <div class="col-lg-9">

                    <div class="row">

                        <div class="col-md-12">
                            <div class="card" >
                                <div class="card-body" >
                                    <h6 class="card-title"> Data Statistical Distribution </h6>
                                   <center><span><h2 id="producthead" style="color: #217345"></h2></span></center>
                                </div>
                                <div class="card-body">
                                    <table class="table table-hover table-inverse">
                                        <thead>
                                        <tr>

                                            <th>Date</th>
                                            <th>Daily Measure</th>
                                            <th>Daily production</th>
                                            <th>Cummulative Production</th>
                                            <th>Daily Sale</th>
                                            <th>Daily Balance</th>
                                        </tr>
                                        </thead>
                                        <tbody id="databyproduct"></tbody>

                                        <tfoot id="producttotals"></tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>

        </div>
    </div>

    @include('template.logoutView')

    @include('template.createuserView')

    @include('template.changepasswordview')




    <script src="assets/js/vendor.min.js"></script>
    <script src="assets/js/elephant.min.js"></script>
    <script src="assets/js/application.min.js"></script>
    <script src="assets/js/demo.min.js"></script>
    <script src="assets/parsley/js/parsley.min.js"></script>
    <!-- Datejs -->

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-83990101-1', 'auto');
        ga('send', 'pageview');
    </script>


<script>

    $(document).on('change','#byproduct',function(e) {
        var product_id = $(this).val();
        $.get("{{url('getProductName')}}/"+product_id, function(data) {
            $('#producthead').html(data);
        });

        $.ajax({
            type: 'POST',
            url: '{{URL::to('getproductdetails')}}',
            data: {
                '_token': "{{ csrf_token() }}",
                'product_id': product_id
            },
            success: function (data) {
                $('#databyproduct').empty();
                $('#producttotals').empty();
                var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
              var obj = JSON.parse(data);
                var total_daily_measure=0;
                var total_daily_production=0;
                var total_cummulative_production=0;
                var total_daily_sale=0;
                var total_daily_balance=0;
                for(var x in obj){

                    total_daily_measure=parseFloat(total_daily_measure) + parseFloat(obj[x].daily_measure);
                    total_daily_production=parseFloat(total_daily_production) + parseFloat(obj[x].daily_production);
                    total_cummulative_production=parseFloat(total_cummulative_production) + parseFloat(obj[x].cummulative_production);
                    total_daily_sale=parseFloat(total_daily_sale) + parseFloat(obj[x].daily_sale);
                    total_daily_balance=parseFloat(total_daily_balance) + parseFloat(obj[x].daily_balance);
                      var formatdate=new Date(obj[x].name).toString().substring(0, 15);

                    var strToAdd='<tr class="item">' +
                            '<td><div class="form-group">'+formatdate+' </div></td>' +
                            '<td><div class="form-group">'+obj[x].daily_measure+' </div></td>' +
                            '<td><div class="form-group">'+obj[x].daily_production+' </div></td>' +
                            '<td><div class="form-group">'+obj[x].cummulative_production+' </div> </td> ' +
                            '<td><div class="form-group">'+obj[x].daily_sale+' </div> </td> ' +
                            '<td><div class="form-group">'+obj[x].daily_balance+'</div> </td>' +
                            ' </tr>';


                    $('#databyproduct').append(strToAdd);
                }
                   var footervalues='  <tr class="item" style="font-weight: bold;">' +
                           '<td style="background-color:  #217345;color: white"><div class="form-group"> TOTAL</div></td>' +
                           '<td style="background-color:  #217345;color: white"><div class="form-group">'+total_daily_measure.toFixed(3)+' </div></td>' +
                           '<td style="background-color:  #217345;color: white"><div class="form-group">'+total_daily_production.toFixed(3)+' </div></td>' +
                           '<td style="background-color:  #217345;color: white"><div class="form-group"></div> </td> ' +
                           '<td style="background-color:  #217345;color: white"><div class="form-group">'+total_daily_sale.toFixed(3)+' </div> </td> ' +
                           '<td style="background-color:  #217345;color: white"><div class="form-group"></div> </td>' +
                           ' </tr>';
                $('#producttotals').append(footervalues);


            }
        })
    });
</script>


</div>
</body>
</html>