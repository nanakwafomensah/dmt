<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>  DMT Crude Oil processing</title>
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta property="og:url" content="">
    <meta property="og:type" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:creator" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
    <link rel="apple-touch-icon" sizes="180x180" href="">
    <link rel="icon" type="image/png" href="favicon.ico" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon.ico" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#27ae60">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="assets/css/vendor.min.css">
    <link rel="stylesheet" href="assets/css/elephant.min.css">
    <link rel="stylesheet" href="assets/css/application.min.css">
    <link rel="stylesheet" href="assets/css/demo.min.css">
    <link rel="stylesheet" href="assets/parsley/css/parsley.css">

</head>
<body class="layout layout-header-fixed">
<div class="layout-header">
    @include('template.topbar')
</div>
<div class="layout-main">
    <div class="layout-sidebar" >
        <div class="layout-sidebar-backdrop" style="background-color: #217345;"></div>
        <div class="layout-sidebar-body" style="background-color: #217345;">
            <div class="custom-scrollbar" >
                <nav id="sidenav" class="sidenav-collapse collapse" style="background-color: #217345; color: #fff">
                    <ul class="sidenav">
                        <li class="sidenav-heading">Production Setting</li>
                        <li class="sidenav-item ">
                            <a href="vessel" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-truck"></span>
                                <span class="sidenav-label" style="font-size: 11px">Vessel</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="tank" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-beer"></span>
                                <span class="sidenav-label" style="font-size: 11px">Tank</span>
                            </a>

                        </li>

                        <li class="sidenav-item ">
                            <a href="product" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-group"></span>
                                <span class="sidenav-label" style="font-size: 11px">Refined Product</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="client" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-user"></span>
                                <span class="sidenav-label" style="font-size: 11px">Off Takers</span>
                            </a>

                        </li>




                        <li class="sidenav-heading">Depot Record</li>
                        <li class="sidenav-item ">
                            <a href="consignment" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">New Crude Oil </span>
                            </a>

                        </li>
                        <li class="sidenav-item active">
                            <a href="refined" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-plus-circle"></span>
                                <span class="sidenav-label" style="font-size: 11px">New Product</span>
                            </a>

                        </li>

                        <li class="sidenav-heading">Sale</li>
                        <li class="sidenav-item">
                            <a href="request" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-archive"></span>
                                <span class="sidenav-label" style="font-size: 11px">New Release</span>
                            </a>
                        </li>
                        <li class="sidenav-item">
                            <a href="load" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-suitcase"></span>
                                <span class="sidenav-label" style="font-size: 11px">Loading</span>
                            </a>
                        </li>
                        <li class="sidenav-heading">Report</li>
                        <li class="sidenav-item ">
                            <a href="report" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Summary Report</span>
                            </a>
                        </li>
                        <li class="sidenav-item ">
                            <a href="reportother" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Other Report</span>
                            </a>
                        </li>
                        <li class="sidenav-item">
                            <a href="remark" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-list"></span>
                                <span class="sidenav-label" style="font-size: 11px">Remarks</span>
                            </a>
                        </li>

                        <li class="sidenav-heading">User Management</li>

                        <li class="sidenav-item ">
                            <a href="#createUser" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-user"></span>
                                <span class="sidenav-label" style="font-size: 11px" data-toggle="modal" data-target="#createUser">Create New User</span>
                            </a>

                        </li>
                        <li class="sidenav-item ">
                            <a href="userutil" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-edit"></span>
                                <span class="sidenav-label" style="font-size: 11px" >User Utilities</span>
                            </a>

                        </li>

                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">

                <h1 class="title-bar-title">
                    <span class="d-ib">Refined Product </span>

                </h1>
                <p class="title-bar-description">
                    <small  style ="color:#217345">Utilities</small>

                </p>
            </div>


            <div class="pull-left">
                <button class=" btn  btn-success" style = "text-transform: capitalize; background-color: #217345" data-toggle="modal" data-target="#newDistrict" type="button"> <i class = "icon icon-plus-circle"></i> &nbsp New Product</button>


            </div>
            <br><br>
            <div class="row">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-actions">

                                <button type="button" class="card-action card-reload" title="Reload"></button>

                            </div>

                        </div>
                        <div class="card-body">

                            <table id="demo-datatables-responsive-1" class="table table-bordered table-striped table-nowrap dataTable" cellspacing="0" width="100%" style="font-size: 12px">
                                <thead>
                                <tr>
                                    <th> Date</th>
                                    <th> product </th>
                                    <th> Tank From</th>
                                    <th> Tank</th>
                                    <th> Vessel</th>
                                    <th> Daily Measure</th>

                                    <th> Daily Production</th>
                                    <th> Daily Sale</th>
                                    <th> Gov quantity</th>
                                    <th> Remark</th>
                                    <th><center>Action</center></th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php $x=1; ?>
                                @foreach(\App\Byproduct::all() as $p)

                                    <tr>
                                        <td>
                                            {{date("D M jS, Y", strtotime($p->name))}}
                                        </td>
                                        <td>
                                            {{\App\Product::find($p->product_id)->name}}
                                        </td>
                                        <td>
                                            {{\App\Tank::find($p->tank_id)->name}}
                                        </td>
                                        <td>
                                            {{\App\Tank::find($p->by_tank_id)->name}}
                                        </td>
                                        <td>
                                            {{\App\Vessel::find($p->vessel_id)->name}}
                                        </td>
                                        <td>
                                            {{$p->daily_measure}}
                                        </td>
                                        <td>
                                            {{$p->daily_production}}
                                        </td>
                                        <td>
                                            {{$p->daily_sale}}
                                        </td>
                                        <td>
                                            {{$p->gov_quantity}}
                                        </td>
                                        <td>
                                            {{$p->remark}}
                                        </td>

                                        <td>
                                            <center>
                                                <button class="deletebtn btn  btn-warning" style = "text-transform: capitalize;  background-color: maroon; ; border-color: maroon"  type="button" data-toggle="modal" data-target="#delDistrict"
                                                        data-id="{{$p->id}}"
                                                        data-name="{{$p->name}}"

                                                > <i class = "icon icon-trash"></i></button>
                                                <button class="editbtn btn  btn-warning" style = "text-transform: capitalize;  background-color: goldenrod; ; border-color: goldenrod"  type="button" data-toggle="modal" data-target="#editDistrict" data-id="{{$p->id}}" data-name="{{$p->name}}"
                                                        data-daily_measure="{{$p->daily_measure}}"
                                                        data-daily_production="{{$p->daily_production}}"
                                                        data-daily_sale="{{$p->daily_sale}}"
                                                        data-tank_id="{{$p->tank_id}}"
                                                        data-product_id="{{$p->product_id}}"
                                                        data-by_tank_id="{{$p->by_tank_id}}"
                                                        data-gov_quantity="{{$p->gov_quantity}}"
                                                        data-vessel_id="{{$p->vessel_id}}"
                                                        data-remark="{{$p->remark}}"
                                                > <i class = "icon icon-edit"></i></button>
                                            </center>
                                        </td>

                                    </tr>
                                @endforeach
                                <tfoot>
                                <tr>
                                    <td style="background-color:  #217345"></td>
                                    <td  style="background-color:  #217345"></td>
                                    <td  style="background-color:  #217345"></td>
                                    <td  style="background-color:  #217345"></td>
                                    <td style="background-color:  #217345; color: white;"><b>TOTAL</b></td>
                                    <td style="background-color:  #217345 ;color: white;"><b>{{number_format(\App\Byproduct::all()->sum('daily_measure'),3)}}</b></td>

                                    <td style="background-color:  #217345 ;color: white;"><b>{{number_format(\App\Byproduct::all()->sum('daily_production'),3)}}</b></td>
                                    <td style="background-color:  #217345 ;color: white;"><b>{{number_format(\App\Byproduct::all()->sum('daily_sale'),3)}}</b></td>
                                    <td style="background-color:  #217345 ;color: white;"><b>{{number_format(\App\Byproduct::all()->sum('gov_quantity'),3)}}</b></td>
                                    <td style="background-color:  #217345 ;color: white;"><b></b></td>
                                    <td style="background-color:  #217345 ;color: white;"><b></b></td>

                                </tr>
                                </tfoot>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    @include('template.logoutView')

    @include('template.createuserView')

    @include('template.changepasswordview')

    <div id="delDistrict" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <form action="deleterefined" method="POST">
                {{csrf_field()}}
                <div class="modal-content">
                    <div class="modal-header bg-primary" style="background-color: #217345">
                        <button type="button" class="close" data-dismiss="modal" style="color: #fff">
                            <span aria-hidden="true" style="color: #fff">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <div class="text-center">
                            <span class="icon icon-trash icon-5x m-y-lg"></span>
                            <h4 class="modal-title" style="font-size: 14px">Product</h4>

                        </div>
                    </div>
                    <div class="modal-tabs">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#display" data-toggle="tab" style="font-size: 12px">Do You Want To Delete <span id="nameDelete"></span> district From System?</a></li>

                        </ul>
                        <input type="hidden" id="idDelete" name="idDelete"/>

                    </div>
                    <div class="modal-footer">
                        <center><button type="submit" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-check"></i>&nbsp &nbsp Yes &nbsp &nbsp</button>
                            <button type="button" class="btn btn-danger" style="background-color: maroon"><i class="icon icon-close"></i> Cancel</button></center>

                    </div>
                </div>
            </form>
        </div>
    </div>


    <div id="editDistrict" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <form action="updaterefined" method="post" data-parsley-validate="" id="updaterefined">
                {{csrf_field()}}
                <div class="modal-content">
                    <div class="modal-header bg-primary" style="background-color: #217345">
                        <button type="button" class="close" data-dismiss="modal" style="color: #fff" >
                            <span aria-hidden="true" style="color: #fff">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <div class="text-center">
                            <span class="icon icon-edit icon-5x m-y-lg"></span>
                            <h4 class="modal-title" style="font-size: 12px">Edit Refined Details</h4>

                        </div>
                    </div>
                    <div class="modal-tabs">

                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="display3">

                                <div class="form-group">
                                    <div class="row">
                                        <input id="idEdit" name="idEdit" type="hidden"/>
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Name</label>
                                            <input id="nameEdit" name="nameEdit"  class="form-control" type="date" style="font-size: 11px" required="">
                                        </div>
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Vessel</label>
                                            <select id="vessel_idEdit" class="form-control" style="font-size: 11px" name="vessel_idEdit" required>
                                                <option value="" >Select Vessel</option>
                                                @foreach(App\Vessel::all() as $t)
                                                    <option value="{{$t->id}}" >{{$t->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">From Tank</label>
                                            <select id="tank_idEdit" class="form-control" style="font-size: 11px" name="tank_idEdit" required>
                                                <option value="" >Select Tank</option>
                                                @foreach(App\Tank::all()->where('purpose','1') as $t)
                                                    <option value="{{$t->id}}" >{{$t->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Product</label>
                                            <select id="product_idEdit" class="form-control" style="font-size: 11px"  onchange="getval(this);" name="product_idEdit" required>
                                                <option value="" >Select Product</option>
                                                @foreach(App\Product::all() as $t)
                                                    <option value="{{$t->id}}" >{{$t->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Refined/byproduct</label>
                                            <select id="by_tank_idEdit" class="form-control" style="font-size: 11px"  name="by_tank_idEdit" required readonly="">
                                                <option value="" >Select Tank</option>
                                                @foreach(App\Tank::where('purpose','2')->orWhere('purpose','3')->get() as $t)
                                                    <option value="{{$t->id}}" >{{$t->name}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Daily Production</label>
                                            <input id="daily_productionEdit" name="daily_productionEdit"  class="form-control" type="number"min="0" value="0" step="0.001" style="font-size: 11px" required="">

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Daily Sale</label>
                                            <input id="daily_saleEdit" name="daily_saleEdit"  class="form-control" type="number"min="0" value="0" step="0.001" style="font-size: 11px" required="">

                                        </div>
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Daily measure</label>
                                            <input id="daily_measureEdit" name="daily_measureEdit"  class="form-control" type="number"min="0" value="0" step="0.001" style="font-size: 11px" required="">

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">gov Quantity</label>
                                            <input id="gov_quantityEdit" name="gov_quantityEdit"  class="form-control" type="number"min="0" value="0" step="0.001" style="font-size: 11px" required="">

                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-6">
                                                <label  class="form-label" style="font-size: 12px">Remarks</label>
                                                <textarea name="remarkEdit" id="remarkEdit" class="form-control"></textarea>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="submit" id="savee" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-save"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="newDistrict" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <form action="saverefined" method="post" data-parsley-validate="" id="editform">
                {{csrf_field()}}
                <div class="modal-content">
                    <div class="modal-header bg-primary" style="background-color: #217345">
                        <button type="button" class="close" data-dismiss="modal" style="color: #fff" >
                            <span aria-hidden="true" style="color: #fff">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <div class="text-center">
                            <span class="icon icon-edit icon-5x m-y-lg"></span>
                            <h4 class="modal-title" style="font-size: 12px">Add Refined</h4>

                        </div>
                    </div>
                    <div class="modal-tabs">

                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="display3">

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Name</label>
                                            <input id="name" name="name" value="{{old('name')}}" class="form-control" type="date" style="font-size: 11px" required="">
                                        </div>
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Vessel</label>
                                            <select id="vessel_id" class="form-control" style="font-size: 11px" name="vessel_id" required>
                                                <option value="" >Select Vessel</option>
                                                @foreach(App\Vessel::all() as $t)
                                                    <option value="{{$t->id}}" {{ old('vessel_id')==$t->id ? 'selected' : ''  }} >{{$t->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">From Tank</label>
                                                <select id="tank_id" class="form-control" style="font-size: 11px" name="tank_id" required>
                                                    <option value="" >Select Tank</option>
                                                    @foreach(App\Tank::all()->where('purpose','1') as $t)
                                                        <option value="{{$t->id}}" {{ old('tank_id')==$t->id ? 'selected' : ''  }} >{{$t->name}}</option>
                                                    @endforeach
                                                </select>
                                             </div>
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Product</label>
                                            <select id="product_id" class="form-control" onchange="getval(this);" style="font-size: 11px" name="product_id" required>
                                                <option value="" >Select Product</option>
                                                @foreach(App\Product::all() as $t)
                                                    <option value="{{$t->id}}" {{ old('product_id')==$t->id ? 'selected' : ''  }}>{{$t->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Refined/byproduct</label>
                                            <select id="by_tank_id" class="form-control" style="font-size: 11px" name="by_tank_id" required readonly="">
                                                <option value="" >Select Tank</option>
                                                @foreach(App\Tank::where('purpose','2')->orWhere('purpose','3')->get() as $t)
                                                    <option value="{{$t->id}}" {{ old('by_tank_id')==$t->id ? 'selected' : ''  }} >{{$t->name}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Daily Production</label>
                                            <input id="daily_production" name="daily_production"  class="form-control" type="number"min="0" value="0" step="0.001" style="font-size: 11px" required="">

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Daily Sale</label>
                                            <input id="daily_sale" name="daily_sale"  class="form-control" type="number"min="0" value="0" step="0.001" style="font-size: 11px" required="">

                                        </div>
                                        <div class="col-md-6">
                                            <label  class="form-label" style="font-size: 12px">Daily measure</label>
                                            <input id="daily_measure" name="daily_measure"  class="form-control" type="number"min="0" value="0" step="0.001" style="font-size: 11px" required="">

                                        </div>
                                    </div>
                                      <div class="row">
                                          <div class="col-md-6">
                                              <label  class="form-label" style="font-size: 12px">gov Quantity</label>
                                              <input id="gov_quantity" name="gov_quantity"  class="form-control" type="number"min="0" value="0" step="0.001" style="font-size: 11px" required="">

                                          </div>
                                          <div class="col-md-6">
                                              <label  class="form-label" style="font-size: 12px">Remarks</label>
                                              <textarea name="remark" id="remark" class="form-control"></textarea>

                                          </div>
                                      </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="submit" id="savee" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-save"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>



    <script src="assets/js/vendor.min.js"></script>
    <script src="assets/js/elephant.min.js"></script>
    <script src="assets/js/application.min.js"></script>
    <script src="assets/js/demo.min.js"></script>
    <script src="assets/parsley/js/parsley.min.js"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-83990101-1', 'auto');
        ga('send', 'pageview');
    </script>
    <script src="assets/js/toastr.min.js"></script>



    <script>
        $(document).on('click','.editbtn',function(){
            $('#nameEdit').val($(this).data('name'));
            $('#daily_productionEdit').val($(this).data('daily_production'));
            $('#daily_saleEdit').val($(this).data('daily_sale'));
            $('#daily_measureEdit').val($(this).data('daily_measure'));
            $('#gov_quantityEdit').val($(this).data('gov_quantity'));
            $('#idEdit').val($(this).data('id'));
            $('#tank_idEdit').val($(this).data('tank_id')).select();
            $('#product_idEdit').val($(this).data('product_id')).select();
            $('#by_tank_idEdit').val($(this).data('by_tank_id')).select();
            $('#vessel_idEdit').val($(this).data('vessel_id')).select();
            $('#remarkEdit').val($(this).data('remark'));
        });
    </script>
    <script>
        $(document).on('click','.deletebtn',function() {
            $('#idDelete').val($(this).data('id'));
            $("#nameDelete").html($(this).data('name'));

        });

    </script>
    <script>
        function getval(sel)
        {

            var  product_id=sel.value;

            $.ajax({
                type:"GET",
                url:"{{url('selecttankbyproductid')}}/"+product_id,
                success: function(data) {
                    $('#by_tank_id').val(data).change();
                    $('#by_tank_idEdit').val(data).change();
                }
            });
        }
    </script>
    <script>
        $("#updaterefined").submit(function(e){
            e.preventDefault();
            var formdata = $(this).serialize(); // here $(this) refere to the form its submitting

            $.ajax({
                type: 'POST',
                url: "{{ url('/updaterefined') }}",
                data: formdata, // here $(this) refers to the ajax object not form
                success: function (data) {
                    alert("Record Updated Successfully");
                },
            });

        });
    </script>
</body>
</html>