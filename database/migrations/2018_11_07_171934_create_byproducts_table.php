<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateByproductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('byproducts', function (Blueprint $table) {
            $table->increments('id');
            $table->date('name');
            $table->integer('product_id');
//            $table->string('product_name');
            $table->integer('tank_id');
            $table->integer('by_tank_id');
            $table->integer('vessel_id');
            $table->string('daily_measure');
            $table->string('daily_production');
            $table->string('daily_sale');
            $table->string('gov_quantity');
            $table->text('remark')->nullable($value = true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('byproducts');
    }
}
