<?php

use Illuminate\Database\Seeder;

class userSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->delete();
        $credentials=[
            'first_name'=>'Admin',
            'last_name'=>'Mensah',
            'fullname'=>'admin',
            'phonenumber'=>'1111111111',
            'email'=>'admin@gmail.com',
            'password'=>'12345',
            'location'=>"Dansoman",
            'username'=>'admin',
            'sex'=>'M',

        ];
         Sentinel::registerAndActivate($credentials);
    }
}
